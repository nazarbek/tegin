<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat1_id')->unsigned()->index();
            $table->foreign('cat1_id')->references('id')->on('cat1s')->onDelete('cascade');
            $table->integer('cat2_id')->unsigned()->index();
            $table->foreign('cat2_id')->references('id')->on('cat2s')->onDelete('cascade');
            $table->string('title');
            $table->text('description');
            $table->string('img1');
            $table->string('img2');
            $table->string('img3');
            $table->string('img4');
            $table->string('name');
            $table->integer('region_id');
            $table->integer('city_id');
            $table->string('address');
            $table->string('email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
