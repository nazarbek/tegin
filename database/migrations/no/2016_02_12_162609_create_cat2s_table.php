<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCat2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat2s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat1_id')->unsigned()->index();
            $table->foreign('cat1_id')->references('id')->on('cat1s')->onDelete('cascade');
            $table->string('name_kz');
            $table->string('name_ru');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cat2s');
    }
}
