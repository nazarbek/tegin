<?php
use App\Region;
use App\City;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r1 = new Region;
        $r2 = new Region;
        $r3 = new Region;
        $r4 = new Region;
        $r5 = new Region;
        $r6 = new Region;
        $r7 = new Region;
        $r8 = new Region;
        $r9 = new Region;
        $r10 = new Region;
        $r11 = new Region;
        $r12 = new Region;
        $r13 = new Region;
        $r14 = new Region;

        $r1->name_ru = 'Акмолинская область';
        $r1->name_kz = 'Ақмола облысы';

		$r2->name_ru = 'Актюбинская область';
		$r2->name_kz = 'Ақтөбе облысы';

		$r3->name_ru = 'Алматинская область';
		$r3->name_kz = 'Алматы облысы';

		$r4->name_ru = 'Атырауская область';
		$r4->name_kz = 'Атырау облысы';

		$r5->name_ru = 'Восточно-Казахстанская область';
		$r5->name_kz = 'Шығыс Қазақстан облысы';

		$r6->name_ru = 'Жамбылская область';
		$r6->name_kz = 'Жамбыл облысы';

		$r7->name_ru = 'Западно-Казахстанская область';
		$r7->name_kz = 'Батыс Қазақстан область';

		$r8->name_ru = 'Карагандинская область';
		$r8->name_kz = 'Қарағанды облысы';

		$r9->name_ru = 'Костанайская область';
		$r9->name_kz = 'Қостанай облысы';

		$r10->name_ru = 'Кызылординская область';
		$r10->name_kz = 'Қызылорда облысы';

		$r11->name_ru = 'Мангистауская область';
		$r11->name_kz = 'Маңғыстау облысы';

		$r12->name_ru = 'Павлодарская область';
		$r12->name_kz = 'Павлодар облысы';

		$r13->name_ru = 'Северо-Казахстанская область';
		$r13->name_kz = 'Солтүстік Қазақстан облысы';

		$r14->name_ru = 'Южно-Казахстанская область';
		$r14->name_kz = 'Оңтүстік Қазақстан облысы';

		$r1->save();
		$r2->save();
		$r3->save();
		$r4->save();
		$r5->save();
		$r6->save();
		$r7->save();
		$r8->save();
		$r9->save();
		$r10->save();
		$r11->save();
		$r12->save();
		$r13->save();
		$r14->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;
		$c6 = new City;
		$c7 = new City;

		$c1->name_ru = 'Шымкент';
		$c1->name_kz = 'Шымкент';
		$c1->region_id = $r14->id;
		$c2->name_ru = 'Кентау';
		$c2->name_kz = 'Кентау';
		$c2->region_id = $r14->id;
		$c3->name_ru = 'Сарыагаш';
		$c3->name_kz = 'Сарыағаш';
		$c3->region_id = $r14->id;
		$c4->name_ru = 'Жетысай';
		$c4->name_kz = 'Жетісай';
		$c4->region_id = $r14->id;
		$c5->name_ru = 'Шардара';
		$c5->name_kz = 'Шардара';
		$c5->region_id = $r14->id;
		$c6->name_ru = 'Арыс';
		$c6->name_kz = 'Арыс';
		$c6->region_id = $r14->id;
		$c7->name_ru = 'Ленгер';
		$c7->name_kz = 'Леңгір';
		$c7->region_id = $r14->id;

		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();
		$c6->save();
		$c7->save();


		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;

		$c1->name_ru = 'Петропавл';
		$c1->name_kz = 'Петропавл';
		$c1->region_id = $r13->id;
		$c2->name_ru = 'Тайынша';
		$c2->name_kz = 'Тайынша';
		$c2->region_id = $r13->id;
		$c3->name_ru = 'Булаево';
		$c3->name_kz = 'Булаево';
		$c3->region_id = $r13->id;
		$c4->name_ru = 'Сергеевка';
		$c4->name_kz = 'Сергеевка';
		$c4->region_id = $r13->id;
		$c5->name_ru = 'Мамлютка';
		$c5->name_kz = 'Мамлютка';
		$c5->region_id = $r13->id;

		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;

		$c1->name_ru = 'Павлодар';
		$c1->name_kz = 'Павлодар';
		$c1->region_id = $r12->id;
		$c2->name_ru = 'Экибастуз';
		$c2->name_kz = 'Екібастұз';
		$c2->region_id = $r12->id;
		$c3->name_ru = 'Аксу';
		$c3->name_kz = 'Ақсу';
		$c3->region_id = $r12->id;

		$c1->save();
		$c2->save();
		$c3->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;

		$c1->name_ru = 'Актау';
		$c1->name_kz = 'Ақтау';
		$c1->region_id = $r11->id;
		$c2->name_ru = 'Жанаозен';
		$c2->name_kz = 'Жаңаөзен';
		$c2->region_id = $r11->id;
		$c3->name_ru = 'Форт-Шевченко';
		$c3->name_kz = 'Форт-Шевченко';
		$c3->region_id = $r11->id;

		$c1->save();
		$c2->save();
		$c3->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c1->name_ru = 'Кызылорда';
		$c1->name_kz = 'Қызылорда';
		$c1->region_id = $r10->id;
		$c2->name_ru = 'Байконур';
		$c2->name_kz = 'Байқоңыр';
		$c2->region_id = $r10->id;
		$c3->name_ru = 'Аральск';
		$c3->name_kz = 'Арал';
		$c3->region_id = $r10->id;
		$c4->name_ru = 'Казалинск';
		$c4->name_kz = 'Қазалы';
		$c4->region_id = $r10->id;

		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();		

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;
		$c1->name_ru = 'Костанай';
		$c1->name_kz = 'Қостанай';
		$c1->region_id = $r9->id;
		$c2->name_ru = 'Рудный';
		$c2->name_kz = 'Рудный';
		$c2->region_id = $r9->id;
		$c3->name_ru = 'Лисаковск';
		$c3->name_kz = 'Лисаковск';
		$c3->region_id = $r9->id;
		$c4->name_ru = 'Житикара';
		$c4->name_kz = 'Жетіқара';
		$c4->region_id = $r9->id;
		$c5->name_ru = 'Аркалык';
		$c5->name_kz = 'Арқалық';
		$c5->region_id = $r9->id;

		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;
		$c6 = new City;
		$c7 = new City;
		$c8 = new City;
		$c9 = new City;
		$c10 = new City;
		$c11 = new City;
		$c1->name_ru = 'Караганда';
		$c1->name_kz = 'Қарағанды';
		$c1->region_id = $r8->id;
		$c2->name_ru = 'Темиртау';
		$c2->name_kz = 'Теміртау';
		$c2->region_id = $r8->id;
		$c3->name_ru = 'Жезказган';
		$c3->name_kz = 'Жезқазған';
		$c3->region_id = $r8->id;
		$c4->name_ru = 'Балхаш';
		$c4->name_kz = 'Балқаш';
		$c4->region_id = $r8->id;
		$c5->name_ru = 'Сатпаев';
		$c5->name_kz = 'Сәтпаев';
		$c5->region_id = $r8->id;
		$c6->name_ru = 'Сарань';
		$c6->name_kz = 'Саран';
		$c6->region_id = $r8->id;
		$c7->name_ru = 'Шахтинск';
		$c7->name_kz = 'Шахтинск';
		$c7->region_id = $r8->id;
		$c8->name_ru = 'Абай';
		$c8->name_kz = 'Абай';
		$c8->region_id = $r8->id;
		$c9->name_ru = 'Приозёрск';
		$c9->name_kz = 'Приозёрск';
		$c9->region_id = $r8->id;
		$c10->name_ru = 'Каражал';
		$c10->name_kz = 'Қаражал';
		$c10->region_id = $r8->id;
		$c11->name_ru = 'Каркаралинск';
		$c11->name_kz = 'Қарқаралы';
		$c11->region_id = $r8->id;

		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();
		$c6->save();
		$c7->save();
		$c8->save();
		$c9->save();
		$c10->save();
		$c11->save();

		$c1 = new City;
		$c2 = new City;
		$c1->name_ru = 'Уральск';
		$c1->name_kz = 'Орал';
		$c1->region_id = $r7->id;
		$c2->name_ru = 'Аксай';
		$c2->name_kz = 'Ақсай';
		$c2->region_id = $r7->id;
		$c1->save();
		$c2->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c1->name_ru = 'Тараз';
		$c1->name_kz = 'Тараз';
		$c1->region_id = $r6->id;
		$c2->name_ru = 'Шу';
		$c2->name_kz = 'Шу';
		$c2->region_id = $r6->id;
		$c3->name_ru = 'Каратау';
		$c3->name_kz = 'Қаратау';
		$c3->region_id = $r6->id;
		$c4->name_ru = 'Жанатас';
		$c4->name_kz = 'Жаңатас';
		$c4->region_id = $r6->id;
		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;
		$c6 = new City;
		$c7 = new City;
		$c8 = new City;
		$c9 = new City;
		$c10 = new City;
		$c1->name_ru = 'Семей';
		$c1->name_kz = 'Семей';
		$c1->region_id = $r5->id;
		$c2->name_ru = 'Усть-Каменогорск';
		$c2->name_kz = 'Усть-Каменогорск';
		$c2->region_id = $r5->id;
		$c3->name_ru = 'Риддер';
		$c3->name_kz = 'Риддер';
		$c3->region_id = $r5->id;
		$c4->name_ru = 'Зыряновск';
		$c4->name_kz = 'Зыряновск';
		$c4->region_id = $r5->id;
		$c5->name_ru = 'Аягуз';
		$c5->name_kz = 'Аягуз';
		$c5->region_id = $r5->id;
		$c6->name_ru = 'Шемонаиха';
		$c6->name_kz = 'Шемонаиха';
		$c6->region_id = $r5->id;
		$c7->name_ru = 'Зайсан';
		$c7->name_kz = 'Зайсан';
		$c7->region_id = $r5->id;
		$c8->name_ru = 'Курчатов';
		$c8->name_kz = 'Курчатов';
		$c8->region_id = $r5->id;
		$c9->name_ru = 'Серебрянск';
		$c9->name_kz = 'Серебрянск';
		$c9->region_id = $r5->id;
		$c10->name_ru = 'Чарск';
		$c10->name_kz = 'Чарск';
		$c10->region_id = $r5->id;

		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();
		$c6->save();
		$c7->save();
		$c8->save();
		$c9->save();
		$c10->save();

		$c1 = new City;
		$c2 = new City;
		$c1->name_ru = 'Атырау';
		$c1->name_kz = 'Атырау';
		$c1->region_id = $r4->id;
		$c2->name_ru = 'Кульсары';
		$c2->name_kz = 'Құлсары';
		$c2->region_id = $r4->id;
		$c1->save();
		$c2->save();

		$c1 = new City;
		$c0 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;
		$c6 = new City;
		$c7 = new City;
		$c8 = new City;
		$c9 = new City;
		$c10 = new City;
		$c0->name_ru = 'Алматы';
		$c0->name_kz = 'Алматы';
		$c0->region_id = $r3->id;
		$c1->name_ru = 'Талдыкорган';
		$c1->name_kz = 'Талдықорған';
		$c1->region_id = $r3->id;
		$c2->name_ru = 'Каскелен';
		$c2->name_kz = 'Қаскелең';
		$c2->region_id = $r3->id;
		$c3->name_ru = 'Талгар';
		$c3->name_kz = 'Талғар';
		$c3->region_id = $r3->id;
		$c4->name_ru = 'Капчагай';
		$c4->name_kz = 'Қапшағай';
		$c4->region_id = $r3->id;
		$c5->name_ru = 'Жаркент';
		$c5->name_kz = 'Жаркент';
		$c5->region_id = $r3->id;
		$c6->name_ru = 'Есик';
		$c6->name_kz = 'Есік';
		$c6->region_id = $r3->id;
		$c7->name_ru = 'Текели';
		$c7->name_kz = 'Текелі';
		$c7->region_id = $r3->id;
		$c8->name_ru = 'Уштобе';
		$c8->name_kz = 'Үштөбе';
		$c8->region_id = $r3->id;
		$c9->name_ru = 'Ушарал';
		$c9->name_kz = 'Үшарал';
		$c9->region_id = $r3->id;
		$c10->name_ru = 'Сарканд';
		$c10->name_kz = 'Сарқант';
		$c10->region_id = $r3->id;
		$c0->save();
		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();
		$c6->save();
		$c7->save();
		$c8->save();
		$c9->save();
		$c10->save();

		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;
		$c6 = new City;
		$c7 = new City;
		$c8 = new City;
		$c1->name_ru = 'Актобе';
		$c1->name_kz = 'Ақтөбе';
		$c1->region_id = $r2->id;
		$c2->name_ru = 'Кандыагаш';
		$c2->name_kz = 'Қандыағаш';
		$c2->region_id = $r2->id;
		$c3->name_ru = 'Шалкар';
		$c3->name_kz = 'Шалқар';
		$c3->region_id = $r2->id;
		$c4->name_ru = 'Хромтау';
		$c4->name_kz = 'Хромтау';
		$c4->region_id = $r2->id;
		$c5->name_ru = 'Алга';
		$c5->name_kz = 'Алға';
		$c5->region_id = $r2->id;
		$c6->name_ru = 'Эмба';
		$c6->name_kz = 'Эмба';
		$c6->region_id = $r2->id;
		$c7->name_ru = 'Темир';
		$c7->name_kz = 'Темір';
		$c7->region_id = $r2->id;
		$c8->name_ru = 'Жем';
		$c8->name_kz = 'Жем';
		$c8->region_id = $r2->id;

		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();
		$c6->save();
		$c7->save();
		$c8->save();

		$c0 = new City;
		$c1 = new City;
		$c2 = new City;
		$c3 = new City;
		$c4 = new City;
		$c5 = new City;
		$c6 = new City;
		$c7 = new City;
		$c8 = new City;
		$c9 = new City;
		$c10 = new City;
		$c0->name_ru = 'Астана';
		$c0->name_kz = 'Астана';
		$c0->region_id = $r1->id;
		$c1->name_ru = 'Кокшетау';
		$c1->name_kz = 'Көкшетау';
		$c1->region_id = $r1->id;
		$c2->name_ru = 'Степногорск';
		$c2->name_kz = 'Степногорск';
		$c2->region_id = $r1->id;
		$c3->name_ru = 'Щучинск';
		$c3->name_kz = 'Щучинск';
		$c3->region_id = $r1->id;
		$c4->name_ru = 'Атбасар';
		$c4->name_kz = 'Атбасар';
		$c4->region_id = $r1->id;
		$c5->name_ru = 'Макинск';
		$c5->name_kz = 'Макинск';
		$c5->region_id = $r1->id;
		$c6->name_ru = 'Акколь';
		$c6->name_kz = 'Ақкөл';
		$c6->region_id = $r1->id;
		$c7->name_ru = 'Ерейментау';
		$c7->name_kz = 'Ерейментау';
		$c7->region_id = $r1->id;
		$c8->name_ru = 'Есиль';
		$c8->name_kz = 'Есіл';
		$c8->region_id = $r1->id;
		$c9->name_ru = 'Державинск';
		$c9->name_kz = 'Державинск';
		$c9->region_id = $r1->id;
		$c10->name_ru = 'Степняк';
		$c10->name_kz = 'Степняк';
		$c10->region_id = $r1->id;
		$c0->save();
		$c1->save();
		$c2->save();
		$c3->save();
		$c4->save();
		$c5->save();
		$c6->save();
		$c7->save();
		$c8->save();
		$c9->save();
		$c10->save();

    }
}
