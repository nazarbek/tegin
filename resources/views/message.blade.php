@extends('main')
@section('head')
  <title>
  @if($lang)
    Предложить идею для сайта tegyn.kz
  @else
    tegyn.kz сайтына ұсыныс жасау
  @endif
  </title>
  <script language="JavaScript" src="{{ asset('js/gen_validatorv4.js') }}" type="text/javascript" xml:space="preserve"></script>
@stop
@section('content')
    <div class='container'>
      <div id='report' class='col-md-12'>
        <form id='mform' class="form-horizontal" action='{!! route("message_add") !!}'>
          <br/>
        <fieldset>
        @if($lang)
        <legend>Предложения</legend>
        <div class="daite col-md-12">                     
          <p>Есть какие-то идеи? Напишите нам.</p>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Имя <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <input type='text' class="form-control" name="name"></input>
            <div id='mform_name_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Email <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <input type='email' class="form-control" name="email"></input>
            <div id='mform_email_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Описание <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <textarea class="form-control" name="text"></textarea>
            <div id='mform_text_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" ></label>
          <div class="col-md-4">
            <button id="submit_message" name="message_submit" class="btn btn-primary">Отправить</button>
          </div>
        </div>
        @else
        <legend>Ұсыныс</legend>
        <div class="daite col-md-12">                     
          <p>Егер ұсыныстарыңыз болса бізге жолдаңыз.</p>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Атыңыз <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <input type='text' class="form-control" name="name"></input>
            <div id='mform_name_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Эл. пошта <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <input type='email' class="form-control" name="email"></input>
            <div id='mform_email_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Сипаттама <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <textarea class="form-control" name="text"></textarea>
            <div id='mform_text_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" ></label>
          <div class="col-md-4">
            <button id="submit_message" name="message_submit" class="btn btn-primary">Жолдау</button>
          </div>
        </div>
        @endif
        </fieldset>
        </form>
      </div>
    </div>
    <script language="JavaScript" type="text/javascript"
    xml:space="preserve">//<![CDATA[
    //You should create the validator only after the definition of the HTML form
    var frmvalidator  = new Validator("mform");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    @if($lang)
    frmvalidator.addValidation("name","req","Пожалуйста, введите имя");
    frmvalidator.addValidation("name","maxlen=70",  "Длина имени не может быть больше 70 символов");

    frmvalidator.addValidation("text","req","Пожалуйста, напишите описание");
    frmvalidator.addValidation("text","maxlen=3000",  "Описание не может быть больше 3000 символов");

    frmvalidator.addValidation("email","maxlen=50","Слишком длинный адрес");
    frmvalidator.addValidation("email","req","Пожалуйста, введите адрес эл. почты");
    frmvalidator.addValidation("email","email","Пожалуйста, введите правильный адрес эл. почты");
    @else
    frmvalidator.addValidation("name","req","Өтініш, хабарлама атауын енгізіңіз");
    frmvalidator.addValidation("name","maxlen=70",  "Хабарлама атауы 70 әріптен аспауы тиіс");

    frmvalidator.addValidation("text","req","Өтініш, хабарламаға сипаттама беріңіз");
    frmvalidator.addValidation("text","maxlen=3000",  "Сипаттама 3000 әріптен аспауы тиіс");

    frmvalidator.addValidation("email","maxlen=50","Тым ұзын адрес");
    frmvalidator.addValidation("email","req","Өтініш, поштаңызды енгізіңіз");
    frmvalidator.addValidation("email","email","Өтініш, дұрыс электрондық пошта енгізіңіз");
    @endif
  </script>
@stop