<html lang="en">
  	<head>
    	<meta charset="utf-8">
  	</head>
  	<body>
	<div style='background:#91C6CE;padding-top:10px; padding-bottom:10px; padding-left:5px; padding-right:5px;'>
		<table style='font-family:arial;background:white;padding:1em;margin: 0 auto;'>
			<tr>
				<td>
					<img src='{{ asset("img/logo2.png") }}'></img>
				</td>
			</tr>
			<tr>
				<td>
					<h1 style='font-size:1.7em;'>Здравствуйте, {!! $name !!}</h1>
					<h4>Обновите ваше объявление: </h4>
					<a href='{!! route("ad", [ $ad->id ]) !!}'><h3>{!! $ad->title !!}</h3></a>
					<p>
						Если ваше объявление всё ещё актуально — вы можете продлить ваше объявление еще на 30 дней.
					</p>
				</td>
			</tr>
			<tr>
				<td style='text-align: center;'>
					<a href='{!! route("extend", [$ad->id, $code]) !!}' style='float:left;width:300px;height:40px;margin-top:20px;color:white;text-decoration:none;background: #3873CB;font-size: 1.7em;padding-top:0.3em;'>Продлить</a>
				</td>
			</tr>
			<tr>
				<td>
					<br/>
					<span style='font-size:0.9em;color:gray;'>Это сообщение было отправлено автоматически. Пожалуйста, не отвечайте на него.</span>
				</td>
			</tr>
		</table>
	</div>
	</body>
</html>