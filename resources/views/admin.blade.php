@extends('admin_main')
@section('content')
  <div class='container'>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="{!! route('admin') !!}">Панель администратора</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="{!! route('admin') !!}">Объявления</a></li>
              <li><a href="{!! route('reports') !!}">Жалобы</a></li>
              <li><a href="{!! route('messages') !!}">Письма</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{!! route('logout') !!}">Выйти</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
  </div>
  <div id='list' class='container'>
    @if($ads->first())
      @foreach($ads as $ad)
        <div class='col-md-12 advert'>
          <a href="{!! route('ad', [$ad->id]) !!}">
            <div class='advert-left'>
              <img src='{!! url("photos/".$ad->img1) !!}'></img>
            </div>
            <div class='advert-right1'>
              <h3>{!! substr($ad->title, 0, 55) !!}</h3>
              <p>{!! substr($ad->description, 0, 150) !!}</p>
              <span>{!! $ad->created_at !!}</span>
              <button href='#' class='btn btn-default'>Подробнее</button>
            </div>
          </a>
        </div>
      @endforeach
      {!! $ads->links() !!}
    @endif
  </div>
@stop