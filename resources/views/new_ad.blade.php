@extends('main')
@section('head')
  <title>
  @if($lang)
    Подать новое объявление. Отдам даром Казахстан
  @else
    Жаңа хабарландыру беру. Тегін беремін Қазақстан
  @endif
  </title>
  <script language="JavaScript" src="{{ asset('js/gen_validatorv4.js') }}" type="text/javascript" xml:space="preserve"></script>
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
@stop
@section('content')
	<div class='container'>
    <div id='new-ad-left' class='col-md-8'>
      <h1>
        <?php if($lang){ ?>
        {!! $word->where('name', 'title')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'title')->first()->text_kz !!}
        <?php } ?>
      </h1>
      <form id="myform" class="form-horizontal" action='new_ad/add' method='POST' enctype='multipart/form-data'>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <fieldset>
      <div class="form-group">
        <label class="col-md-2 control-label" for="title">
        <?php if($lang){ ?>
        {!! $word->where('name', 'ad_title')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'ad_title')->first()->text_kz !!}
        <?php } ?>
        <span class='star'>*</span></label>  
        <div class="col-md-8">
        <input id="title" name="title" type="text" placeholder="" class="form-control input-md" >
        <div id='myform_title_errorloc' class="error_strings"></div>
        </div>
      </div>

      <!-- Select Basic -->
      <div id='rubriks' class="form-group">
        <label class="col-md-2 control-label" for="category">
        <?php if($lang){ ?>
        {!! $word->where('name', 'rubrika')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'rubrika')->first()->text_kz !!}
        <?php } ?>
        <span class='star'>*</span></label>
        <div class="col-md-5">
          <select id="category" name="category1" class="form-control">
            @if($lang)
              <option value='0'>Выберите рубрику</option>
            @else
              <option value='0'>Тақырыпты таңдаңыз</option>
            @endif
            @foreach($rubrika as $r)
              @if($lang)
              <option value='{!! $r->id !!}'>{!! $r->name_ru !!}</option>
              @else
              <option value='{!! $r->id !!}'>{!! $r->name_kz !!}</option>
              @endif
            @endforeach
          </select>
          <div id='myform_category1_errorloc' class="error_strings"></div>
        </div>
      </div>
      <div id='rubriks2' class="form-group">
        <label class="col-md-2 control-label" for="category">
        <?php if($lang){ ?>
        {!! $word->where('name', 'podrubrika')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'podrubrika')->first()->text_kz !!}
        <?php } ?>
        <span class='star'>*</span></label>
        <div class="col-md-5">
          <select id="category2" name="category2" class="form-control">
            <option value='0'>Выберите подрубрику</option>
          </select>
          <div id='myform_category2_errorloc' class="error_strings"></div>
        </div>
      </div>

      <!-- Textarea -->
      <div class="form-group">
        <label class="col-md-2 control-label" for="description">
        <?php if($lang){ ?>
        {!! $word->where('name', 'desc')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'desc')->first()->text_kz !!}
        <?php } ?>
        <span class='star'>*</span></label>
        <div class="col-md-8">                     
          <textarea id='description' class="form-control" id="description" name="description"></textarea>
          <div id='myform_description_errorloc' class="error_strings"></div>
        </div>
      </div>
      <div class='col-md-12'>
        <h4>
        <?php if($lang){ ?>
        {!! $word->where('name', 'ad_photo')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'ad_photo')->first()->text_kz !!}
        <?php } ?>
        </h4>
        <div class='col-md-2'>
        </div>
        @if($lang)
        <div id='uploads' class='col-md-7'>
          <div>
            <input type="file" name='file1' class="filestyle" data-placeholder="Не выбран" data-buttonText="Загрузить" data-buttonName="btn-primary">
            <div id='myform_file1_errorloc' class="error_strings"></div>
          </div>
          <div>
            <input type="file" name='file2' class="filestyle" data-placeholder="Не выбран" data-buttonText="Загрузить" data-buttonName="btn-primary">
            <div id='myform_file2_errorloc' class="error_strings"></div>
          </div>
          <div>
            <input type="file" name='file3' class="filestyle" data-placeholder="Не выбран" data-buttonText="Загрузить" data-buttonName="btn-primary">
            <div id='myform_file3_errorloc' class="error_strings"></div>
          </div>
        </div>
        @else
        <div id='uploads' class='col-md-7'>
          <div>
            <input type="file" name='file1' class="filestyle" data-placeholder="Таңдалмады" data-buttonText="Жүктеу" data-buttonName="btn-primary">
            <div id='myform_file1_errorloc' class="error_strings"></div>
          </div>
          <div>
            <input type="file" name='file2' class="filestyle" data-placeholder="Таңдалмады" data-buttonText="Жүктеу" data-buttonName="btn-primary">
            <div id='myform_file2_errorloc' class="error_strings"></div>
          </div>
          <div>
            <input type="file" name='file3' class="filestyle" data-placeholder="Таңдалмады" data-buttonText="Жүктеу" data-buttonName="btn-primary">
            <div id='myform_file3_errorloc' class="error_strings"></div>
          </div>
        </div>
        @endif
      </div>
      
      <div style='margin-bottom:15px;' class='col-md-12'>
        <h3>
        <?php if($lang){ ?>
        {!! $word->where('name', 'contacts')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'contacts')->first()->text_kz !!}
        <?php } ?></h3>
      </div>
      <div class="form-group">
        <label class="col-md-2 control-label" for="name">
        <?php if($lang){ ?>
        {!! $word->where('name', 'name')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'name')->first()->text_kz !!}
        <?php } ?>
        <span class='star'>*</span>
        </label>  
        <div class="col-md-5">
        <input id="name" name="name" type="text" class="form-control input-md">
        <div id='myform_name_errorloc' class="error_strings"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-2 control-label" for="oblast">
        <?php if($lang){ ?>
        {!! $word->where('name', 'region')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'region')->first()->text_kz !!}
        <?php } ?>
          <span class='star'>*</span></label>
        <div class="col-md-5">
          <select id="oblast" name="region" class="form-control">
            <?php if($lang){ ?>
            <option value="0">Выбрать область</option>
            <?php }else{ ?>
            <option value="0">Облысты таңдау</option>
            <?php } ?>
            @foreach($reg as $r)
            <?php if($lang){ ?>
              <option value="{!! $r->id !!}">{!! $r->name_ru !!}</option>
            <?php }else{ ?>
              <option value="{!! $r->id !!}">{!! $r->name_kz !!}</option>
            <?php } ?>
            @endforeach
          </select>
          <div id='myform_region_errorloc' class="error_strings"></div>
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-2 control-label" for="selectbasic">
        <?php if($lang){ ?>
        {!! $word->where('name', 'city')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'city')->first()->text_kz !!}
        <?php } ?>
        </label>
        <div class="col-md-5">
          <select id="gorod" name="city" class="form-control" disabled='disabled'>
            <?php if($lang){ ?>
            <option value="0">Выбрать город</option>
            <?php }else{ ?>
            <option value="0">Қаланы таңдау</option>
            <?php } ?>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-2 control-label" for="adress">
        <?php if($lang){ ?>
        {!! $word->where('name', 'address')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'address')->first()->text_kz !!}
        <?php } ?>
        </label>  
        <div class="col-md-5">
        <input id="adress" name="address" type="text" placeholder="" class="form-control input-md">
          
        </div>
      </div>

      <!-- Text input-->
      <div id='phone-container'>
        <div style='margin-bottom: 8px;' class="form-group">
          <label class="col-md-2 control-label" for="phone1">Телефон <span class='star'>*</span></label>  
          <div class="col-md-3">
          <input id="phone1" class='phone_input' name="phone[1]" type="text" class="form-control input-md"  value='+_ (___) ___ __ __'>
          <div id='myform_phone1_errorloc' class="error_strings"></div>
          </div>
          <div class="col-md-3">
            <label class="checkbox-inline" for="checkboxes-0">
              <input type="checkbox" name="whatsapp[1]" value="1">
              Whatsapp
            </label>
            <label class="checkbox-inline" for="checkboxes-1">
              <input type="checkbox" name="viber[1]" value="2">
              Viber
            </label>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class='col-md-2'></div>
      <div class='col-md-5'>
        <span id='add_new_num'>
        <?php if($lang){ ?>
        {!! $word->where('name', 'ad_number')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'ad_number')->first()->text_kz !!}
        <?php } ?>
        </span>
      </div>
      </div>
      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-2 control-label" for="email">
        <?php if($lang){ ?>
        {!! $word->where('name', 'email')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'email')->first()->text_kz !!}
        <?php } ?>
        <span class='star'>*</span>
        </label>  
        <div class="col-md-4">
        <input id="email" name="email" type="text" placeholder="" class="form-control input-md">
        <div id='myform_email_errorloc' class="error_strings"></div>
        <span class="help-block">
        <?php if($lang){ ?>
        {!! $word->where('name', 'email_help')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'email_help')->first()->text_kz !!}
        <?php } ?>
        </span>
        </div>
      </div>
      <h4 style='margin-top:20px; margin-left: 30px;'>
        @if($lang)
          Вы можете указать свой адрес на карте
        @else
          Өз мекен-жайыңызды картада көрсетсеңіз болады
        @endif
      </h4>
      <div id="map" class='add-map'></div>
      <input name='long' type="hidden" id="long" value="0"/>
      <input name='lat' type="hidden" id="lat" value="0"/>

      <div class="form-group">
        <div class="col-md-2">
        </div>
        <div class="col-md-5">
          <button id="submit" name="submit" class="btn btn-primary">
          <?php if($lang){ ?>
          {!! $word->where('name', 'add_ad')->first()->text_ru !!}
          <?php }else{ ?>
          {!! $word->where('name', 'add_ad')->first()->text_kz !!}
          <?php } ?>
          </button>
        </div>
      </div>
      </fieldset>
      </form>
    </div>
    <div class='col-md-4'>
      <div id='rules'>
        <?php if($lang){ ?>
        {!! $word->where('name', 'rules')->first()->text_ru !!}
        <?php }else{ ?>
        {!! $word->where('name', 'rules')->first()->text_kz !!}
        <?php } ?>
      </div>
    </div>

  </div>
  @foreach($rubrika as $r)
    <?php 
    $curr = $podrubrika->where('cat1_id', $r->id);
    $str = '';
    $str2 = '';
    ?>
    @foreach($curr as $c)
      <?php
      if ($lang) {
        $str = $str.'='.$c->name_ru;
      }else{
        $str = $str.'='.$c->name_kz;
      }
      $str2 = $str2.'='.$c->id;
      ?>
    @endforeach
    <input type="hidden" id="hrub{!! $r->id !!}" value="<?php echo $str;?>"/>
    <input type="hidden" id="hrid{!! $r->id !!}" value="<?php echo $str2;?>"/>
    <?php $str='';?>
  @endforeach

  @foreach($reg as $r)
    <?php 
    $curc = $city->where('region_id', $r->id);
    $str = '';
    $str2 = '';
    ?>
    @foreach($curc as $c)
      <?php
      if ($lang) {
        $str = $str.'='.$c->name_ru;
      }else{
        $str = $str.'='.$c->name_kz;
      }
      $str2 = $str2.'='.$c->id;
      ?>
    @endforeach
    <input type="hidden" id="hcity{!! $r->id !!}" value="<?php echo $str;?>"/>
    <input type="hidden" id="hcid{!! $r->id !!}" value="<?php echo $str2;?>"/>
    <?php $str='';?>
  @endforeach
  <input type="hidden" id="lang" value="{!! $lang !!}"/>
  <script src="{{ asset('js/bootstrap-filestyle.min.js') }}" type="text/javascript" ></script>
  <script type="text/javascript">
  ymaps.ready(init);
  var myMap;
  var lon = document.getElementById('long');
  var lat = document.getElementById('lat');

  function init () {
      myMap = new ymaps.Map("map", {
          center: [48.670376, 67.557491],
          zoom: 4
      });
      var myCollection = new ymaps.GeoObjectCollection();
      myMap.events.add('click', function (e) {
        myCollection.removeAll();
        var coords = e.get('coords');
        myPlacemark = new ymaps.Placemark(coords, {
                hintContent: 'Адрес'
            }
        );
        myCollection.add(myPlacemark);
        myMap.geoObjects.add(myCollection);
        lon.value = coords[0];
        lat.value = coords[1];
      });
  }
  </script>
  <script language="JavaScript" type="text/javascript"
    xml:space="preserve">

    var frmvalidator  = new Validator("myform");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    $("#category").change(function(){
        var v = this.value;
        if(v==0 || v==14 || v==13){
            document.getElementById('rubriks2').style.display='none';
        }else if(v>0&&v<13){
            var s = document.getElementById('hrub'+v).value;
            var s2 = document.getElementById('hrid'+v).value;
            var arr = s.split('=');
            var arr2 = s2.split('=');
            var con = "<option value='0'>Выберите подрубрику</option>";
            if (!lang) {
                con = "<option value='0'>Тақырыпша таңдаңыз</option>";
            }
            
            for (var i = 1; i<arr.length; i++) {
                con+="<option value='"+arr2[i]+"'>"+arr[i]+"</option>";
            }
            $('#category2').html(con);
            document.getElementById('rubriks2').style.display='block';
        }
    });
    $("#category2").change(function(){
        var v2 = this.value;
        var v1 = document.getElementById('category').value;
        if(v==0){
            document.getElementById('rubriks2').style.display='none';
        }
    });

    //You should create the validator only after the definition of the HTML form

    @if($lang)
    frmvalidator.addValidation("title","req","Пожалуйста, введите заголовок");
    frmvalidator.addValidation("title","maxlen=70",  "Длина заголовка не может быть больше 70 символов");

    frmvalidator.addValidation("category","dontselect=0", "Пожалуйста, выберите рубрику");
    //frmvalidator.addValidation("category2","dontselect=0", "Пожалуйста, выберите подрубрику");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '1')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '2')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '3')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '4')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '5')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '6')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '7')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '8')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '9')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '10')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '11')");
    frmvalidator.addValidation("category2","dontselect=0","Пожалуйста, выберите рубрику",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '12')");

    frmvalidator.addValidation("description","req","Пожалуйста, добавьте описание");
    frmvalidator.addValidation("description","maxlen=3000",  "Описание не может быть больше 3000 символов");

    frmvalidator.addValidation("name","req","Пожалуйста, введите имя");
    frmvalidator.addValidation("name","maxlen=70",  "Слишком длинное имя");

    frmvalidator.addValidation("oblast","dontselect=0", "Пожалуйста, укажите область");

    frmvalidator.addValidation("phone1","req","Пожалуйста, введите номер телефона");

    frmvalidator.addValidation("email","maxlen=50","Слишком длинный адрес");
    frmvalidator.addValidation("email","req","Пожалуйста, введите адрес эл. почты");
    frmvalidator.addValidation("email","email","Пожалуйста, введите правильный адрес эл. почты");

    frmvalidator.addValidation("file1","file_extn=jpg;gif;png;jpeg","Разрешенные форматы: jpg;gif;png;jpeg");
    frmvalidator.addValidation("file2","file_extn=jpg;gif;png;jpeg","Разрешенные форматы: jpg;gif;png;jpeg");
    frmvalidator.addValidation("file3","file_extn=jpg;gif;png;jpeg","Разрешенные форматы: jpg;gif;png;jpeg");
    @else
    frmvalidator.addValidation("title","req","Өтініш, хабарлама атауын енгізіңіз");
    frmvalidator.addValidation("title","maxlen=70",  "Хабарлама атауы 70 әріптен аспауы тиіс");

    frmvalidator.addValidation("category","dontselect=0", "Өтініш, тақырып таңдаңыз");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '1')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '2')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '3')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '4')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '5')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '6')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '7')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '8')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '9')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '10')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '11')");
    frmvalidator.addValidation("category2","dontselect=0","Өтініш, тақырыпша таңдаңыз",
        "VWZ_IsListItemSelected(document.forms['myform'].elements['category1'], '12')");

    frmvalidator.addValidation("description","req","Өтініш, хабарламаға сипаттама беріңіз");
    frmvalidator.addValidation("description","maxlen=3000",  "Сипаттама 3000 әріптен аспауы тиіс");

    frmvalidator.addValidation("name","req","Өтініш, атыңызды енгізіңіз");
    frmvalidator.addValidation("name","maxlen=70",  "Есім 70 әріптен аспауы тиіс");

    frmvalidator.addValidation("oblast","dontselect=0", "Өтініш, облысыңызды көрсетіңіз");

    frmvalidator.addValidation("phone1","req","Өтініш, нөміріңізді енгізіңіз");

    frmvalidator.addValidation("email","maxlen=50","Тым ұзын адрес");
    frmvalidator.addValidation("email","req","Өтініш, поштаңызды енгізіңіз");
    frmvalidator.addValidation("email","email","Өтініш, дұрыс электрондық пошта енгізіңіз");

    frmvalidator.addValidation("file1","file_extn=jpg;gif;png;jpeg","Рұқсат етілген формат: jpg;gif;png;jpeg");
    frmvalidator.addValidation("file2","file_extn=jpg;gif;png;jpeg","Рұқсат етілген формат: jpg;gif;png;jpeg");
    frmvalidator.addValidation("file3","file_extn=jpg;gif;png;jpeg","Рұқсат етілген формат: jpg;gif;png;jpeg");
    @endif
  </script>
@stop