@extends('admin_main')
@section('content')
  <div class='container'>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="{!! route('admin') !!}">Панель администратора</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="{!! route('admin') !!}">Объявления</a></li>
              <li><a href="{!! route('reports') !!}">Жалобы</a></li>
              <li class="active"><a href="{!! route('messages') !!}">Предложения</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{!! route('logout') !!}">Выйти</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
  </div>
  <div id='list' class='container'>
    @if($mes->first())
    <table id='report-table'>
      <tr id='rep-first'>
          <td class='rep-id'>ID</td>
          <td class='rep-email'>Эл. почта</td>
          <td class='rep-desc'>Описание</td>
          <td class='rep-data'>Дата отправления</td>
          <td class='rep-del'>Удалить</td>
        </tr>
      @foreach($mes as $ad)
        <tr class='rep-item'>
          <td class='rep-id'>{!! $ad->id !!}</td>
          <td class='rep-email'>{!! $ad->email !!}</td>
          <td class='rep-desc'>{!! $ad->text !!}</td>
          <td class='rep-data'>{!! $ad->created_at !!}</td>
          <td class='rep-del'><a href='#'>X</a></td>
        </tr>
      @endforeach
    </table>
      {!! $mes->links() !!}
    @else
      <h1>Пусто</h1>
    @endif
  </div>
@stop