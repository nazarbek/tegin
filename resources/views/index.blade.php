@extends('main')
@section('head')
<title>
@if($lang)
  Доска объявлений отдам даром и бесплатные услуги в Казахстане
@else
  Қазақстандағы тегін тауарлар мен қызметтерге арналған хабарламалар алаңы
@endif
</title>
<script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
<script type="text/javascript">
  VK.init({apiId: 5356488, onlyWidgets: true});
</script>
<script >
  window.___gcfg = {
    lang: 'ru',
    parsetags: 'onload'
  };
</script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
@stop
@section('content')
	<div id='filter' class='container'>
      <div id='location' class="col-md-4">
        <button id='show-list' class='btn btn-default'>
          <?php
          if ($lang) {
            if ($region==0) {
              echo "Весь Казахстан";
            }else if($region>0){
              if ($city==0) { ?>
              {!! $allreg->where('id', (int)$region)->first()->name_ru !!}
              <?php
              }else{ ?>
              {!! $allcity->where('id', (int)$city)->first()->name_ru !!}
              <?php
              }
            }
          }else{
            if ($region==0) {
              echo "Барлық Қазақстан";
            }else if($region>0){
              if ($city==0) { ?>
              {!! $allreg->where('id', (int)$region)->first()->name_kz !!}
              <?php
              }else{ ?>
              {!! $allcity->where('id', (int)$city)->first()->name_kz !!}
              <?php
              }
            }
          }?>
          <i class="fa fa-chevron-down"></i></button>
        <div id='loc-div'>
          <ul>
            <li data-region='1' data-city='86' class='loc-item2'><a style='margin-left:4%;' href="{!! route('index') !!}?region=1&city=86">Астана</a></li>
            <li data-region='3' data-city='67' class='loc-item2'><a style='margin-left:4%;' href="{!! route('index') !!}?region=3&city=67">Алматы</a></li>
            @if($lang)
            @foreach($allreg as $r)
              <li data-region='{!! $r->id !!}' class='loc-item'>
                <p>{!! $r->name_ru !!}<i class='fa fa-sort-down'></i></p>
                <div class='loc-sec'>
                  <ul>
                    <li data-region='{!! $r->id !!}' class='loc-item2'><a href="{!! route('index') !!}?region={!! $r->id !!}">{!! $r->name_ru !!}</a></li>
                  @foreach($r->city()->get() as $c)
                    @if($c->id!=86&&$c->id!=67)
                    <li data-region='{!! $r->id !!}' data-city='{!! $c->id !!}' class='loc-item2'><a href="{!! route('index') !!}?region={!! $r->id !!}&city={!! $c->id !!}">{!! $c->name_ru !!}</a></li>
                    @endif
                  @endforeach
                  </ul>
                </div>
              </li>
            @endforeach
            @else
            @foreach($allreg as $r)
              <li data-region='{!! $r->id !!}' class='loc-item'>
                <p>{!! $r->name_kz !!}<i class='fa fa-sort-down'></i></p>
                <div class='loc-sec'>
                  <ul>
                    <li data-region='{!! $r->id !!}' class='loc-item2'><a href="{!! route('index') !!}?region={!! $r->id !!}">{!! $r->name_kz !!}</a></li>
                  @foreach($r->city()->get() as $c)
                    @if($c->id!=86&&$c->id!=67)
                    <li data-region='{!! $r->id !!}' data-city='{!! $c->id !!}' class='loc-item2'><a href="{!! route('index') !!}?region={!! $r->id !!}&city={!! $c->id !!}">{!! $c->name_kz !!}</a></li>
                    @endif
                  @endforeach
                  </ul>
                </div>
              </li>
            @endforeach
            @endif
          </ul>
        </div>
      </div>
      <div id='search' class="col-md-8">
        <form action="{!! route('search') !!}">
          <input name="keyword" type="text" placeholder="@if($lang)
Быстрый поиск
@else
Хабарлама іздеу
@endif" class="form-control input-md" required="">
          <button class='btn btn-default'>
          <i class='fa fa-search'></i>
          </button>
        </form>
      </div>
    </div>
    <div id='categories' class='container'>
      <div class='col-md-3'>
        <a href='ad/2' class='main-menu-item'>
          <img style='max-width: 40%; max-height:100px;' src='img/cat03.jpg'></img>
          <h2>
          @if($lang)
            Продукты питания
          @else
            Азық-түлік
          @endif
        </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/3' class='main-menu-item'>
          <img style='max-width: 36%; max-height:100px;' src='img/cat01.png'></img>
          <h2>
          @if($lang)
            Одежда и обувь, аксессуары
          @else
            Киім-кешек, аксессуарлар
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/1' class='main-menu-item'>
          <img style='max-width: 40%; max-heigth:100px;' src='img/cat02.png'></img>
          <h2>
          @if($lang)
            Товары для детей
          @else
            Балаларға арналған тауарлар
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/4' class='main-menu-item'>
          <img style='max-width: 60%; max-height:100px;' src='img/cat05.jpg'></img>
          <h2>
          @if($lang)
            Для дома и дачи
          @else
            Үйге және саяжайға тауарлар
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/5' class='main-menu-item'>
          <img style='max-width: 50%; max-height:80px;' src='img/cat06.jpg'></img>
          <h2>
          @if($lang)
            Электроника и бытовая техника
          @else
            Электроника, тұрмыстық техника
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/6' class='main-menu-item'>
          <img style='max-width:40%; max-height:124px;' src='img/cat08.png'></img>
          <h2>
          @if($lang)
            Животные и растения
          @else
            Жануарлар мен өсімдіктер
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/7' class='main-menu-item'>
          <img style='max-width: 70%; max-height:130px;' src='img/cat07.png'></img>
          <h2>
          @if($lang)
            Хобби и отдых
          @else
            Хобби және демалу
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/8' class='main-menu-item'>
          <img style='max-width: 50%; max-height:90px;' src='img/cat09.png'></img>
          <h2>
          @if($lang)
            Находки и потери
          @else
            Жоғалған, табылған заттар
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/9' class='main-menu-item'>
          <img style='max-width: 61%; max-height:120px;' src='img/cat10.png'></img>
          <h2>
          @if($lang)
            Авто мото техника
          @else
            Авто мото техника
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/11' class='main-menu-item'>
          <img style='max-width: 61%; max-height:120px;' src='img/cat11.jpg'></img>
          <h2>
          @if($lang)
            Бесплатные услуги
          @else
            Тегін қызметтер
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/12' class='main-menu-item'>
          <img style='max-width: 50%; max-height:85px;' src='img/cat12.png'></img>
          <h2>
          @if($lang)
            Медицинская помощь
          @else
            Медициналық көмек
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/13' class='main-menu-item'>
          <img style='max-width: 70%; max-height:130px;' src='img/cat13.jpg'></img>
          <h2>
          @if($lang)
            Финансовая помощь
          @else
            Қаржылай көмек
          @endif
          </h2>
        </a>
      </div>
      <div class='col-md-3'>
        <a href='ad/14' class='main-menu-item'>
          <h2 style='margin-top: 50px;font-size:32px;'>
          @if($lang)
            Прочее
          @else
            Басқа
          @endif
          </h2>
        </a>
      </div>
    </div>
    <div class='container'>
      <div id='main-text' class='col-md-12'>
        @if($lang)
        <h1>Делай людям добро и они ответят тем же!</h1>
        <p>Проект <b>Tegyn.kz</b> это единственный интернет-проект в Казахстане на котором вы можете не только получить 
          товары и услуги абсолютно бесплатно, но так же и помочь другим людям Казахстана, которые возможно очень 
          нуждаются в помощи добрых людей. Помогите проекту развиваться, раскажите своим друзьям и знакомым о самом добром сайте.</p>
        @else
        <h1>Жақсылықтың қайтарымы тек жақсылық!</h1>
        <p><b>Tegyn.kz</b> интернет-проекті барша Қазақстандықтар материалдық көмек пен тегін қызметтер алып және де 
          өзге де мұқтаж жандарға жақсылық жасап өзара көмек көрсету үшін жасалған проект. Таныстарыңыз бен туған-туыстарыңызға
          біздің сайтымыз жайлы айтып, проектінің дамуына өз үлесіңізді қосыңыз.</p>
        @endif
        <div id='social_nets' class='row'>
          <div class='col-md-3'>
            <div id="vk_like"></div>
            <script type="text/javascript">
              VK.Widgets.Like("vk_like", {type: "mini"});
            </script>
          </div>
          <div class='col-md-3'>
            <div class="fb-like" data-href="alber.kz" data-width="40" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
          </div>
          <div class='col-md-3'>
            <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru" data-hashtags="Самый добрый сайт" data-via="">Твитнуть</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
          </div>
          <div class='col-md-3'>
            <div class="g-plusone"></div>
          </div>
        </div>
      </div>
    </div>
    <input type="hidden" id="lang" value="{!! $lang !!}"/>
@stop