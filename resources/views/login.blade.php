@extends('admin_main')
@section('content')
  <div class='container'>
    <form id='login' class="form-horizontal" action="{!! route('check') !!}">
    <fieldset>
    @if($error)
    <div class='error'>
      <p>Неправильный логин или пароль</p>
    </div>
    @endif
    <br/>
    <div class="form-group">
      <label class="col-md-4 control-label" for="textinput">Логин</label>  
      <div class="col-md-4">
      <input id="textinput" name="loginq" type="text" placeholder="" class="form-control input-md" required="">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-4 control-label" for="passwordinput">Пароль</label>
      <div class="col-md-4">
        <input id="passwordinput" name="passwordq" type="password" placeholder="" class="form-control input-md" required="">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-4 control-label" for="singlebutton"></label>
      <div class="col-md-4">
        <button id="singlebutton" name="singlebutton" class="btn btn-primary">войти</button>
      </div>
    </div>
    </fieldset>
    </form>
  </div>
@stop