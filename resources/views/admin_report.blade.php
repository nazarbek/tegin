@extends('admin_main')
@section('content')
  <div class='container'>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="{!! route('admin') !!}">Панель администратора</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="{!! route('admin') !!}">Объявления</a></li>
              <li class="active"><a href="{!! route('reports') !!}">Жалобы</a></li>
              <li><a href="{!! route('messages') !!}">Письма</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{!! route('logout') !!}">Выйти</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
  </div>
  <div id='list' class='container'>
    @if($reports->first())
    <table id='report-table'>
      <tr id='rep-first'>
          <td class='rep-id'>ID</td>
          <td class='rep-email'>Эл. почта</td>
          <td class='rep-desc'>Описание</td>
          <td class='rep-data'>Дата отправления</td>
          <td class='rep-del'>Удалить</td>
        </tr>
      @foreach($reports as $ad)
        <tr class='rep-item'>
          <td class='rep-id'>{!! $ad->id !!}</td>
          <td class='rep-email'>{!! $ad->email !!}</td>
          <td class='rep-desc'>{!! $ad->text !!}</td>
          <td class='rep-data'>{!! $ad->created_at !!}</td>
          <td class='rep-del'><a href='#'>X</a></td>
        </tr>
      @endforeach
    </table>
      {!! $reports->links() !!}
    @endif
  </div>
@stop