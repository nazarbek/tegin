<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('head')

    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.js') }}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div id='bottom-shadow'>
    <div class='container'>
      <div class='col-md-3'>
        <a href="{!! route('index') !!}">
        <img id='logotype' src="{{ asset('img/logo2.png') }}"></img>
        </a>
      </div>
      <div class='col-md-2'>
      </div>
      <div id='head-lang' class='col-md-3'>
        <?php if($lang){ ?>
        <a href='{!! URL::current() !!}?lang=kz' class='btn passive'>Қазақша</a>
        <a class='btn active'>Русский</a>
        <?php }else{ ?>
        <a class='btn active'>Қазақша</a>
        <a href='{!! URL::current() !!}?lang=ru' class='btn passive'>Русский</a>
        <?php } ?>
        
      </div>
      <div class='col-md-4'>
        <a href='{!! route("new") !!}' id='add-new-btn' class='btn btn-success'><i class='fa fa-plus'></i> &nbsp;
          @if($lang)
            Добавить объявление
          @else
            Жаңа хабарландыру
          @endif
          </a>
      </div>
    </div>
  </div>

    @yield('content')

      <!-- ====== Copyright Section ====== -->
    <footer id='footer'>
      <div class="container">
        <!--
        <div class='col-md-2'>
          <a href='#'>
            @if($lang)
              О проекте
            @else
              Проект туралы
            @endif
          </a>

        </div>
        -->
        <div class='col-md-3'>
          <a href='{!! route("message") !!}'>
            @if($lang)
              Предложения
            @else
              Ұсыныс
            @endif
          </a>
        </div>
        
      </div>
    </footer>

    <!-- Bootstrap 3.2.0 js -->
    <script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/my.js') }}"></script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter36256135 = new Ya.Metrika({
                        id:36256135,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/36256135" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
  </body>
</html>