@extends('main')
@section('head')
  <title>
  @if($lang)
    Сообщить о нарушений на сайте tegyn.kz
  @else
    Сайттағы хабарламаға шағымдану
  @endif
  </title>
  <script language="JavaScript" src="{{ asset('js/gen_validatorv4.js') }}" type="text/javascript" xml:space="preserve"></script>
@stop
@section('content')
    <div class='container'>
      <div id='report' class='col-md-12'>
        <form id='mform' class="form-horizontal" action='{!! route("report_add") !!}'>
          <br/>
        <fieldset>
        @if($lang)
        <legend>Сообщить о нарушений</legend>
        <div class="daite col-md-12">                     
          <p>Дайте нам знать, если с объявлением что-то не так. Пожалуйста, опишите в форме ниже, на что в этом объявлении вы жалуетесь.</p>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Описание жалобы <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <textarea class="form-control" name="text"></textarea>
            <div id='mform_text_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" ></label>
          <div class="col-md-4">
            <button id="submit_report" name="report_submit" class="btn btn-primary">Отправить</button>
          </div>
        </div>
        @else
        <legend>Шағым</legend>
        <div class="daite col-md-12">                     
          <p>Егер хабарламада қандай да бір дұрыс емес мәлімет немесе заң бұзушылық байқасаңыз бізге хабарлауыңызды өтінеміз.</p>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" for="textarea" >Шағымның сипаттамасы <span class='star'>*</span></label>
          <div class="col-md-4">                     
            <textarea class="form-control" name="text"></textarea>
            <div id='mform_text_errorloc' class="error_strings"></div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-4 control-label" ></label>
          <div class="col-md-4">
            <button id="submit_report" name="report_submit" class="btn btn-primary" value="{!! $ad_id !!}">Жолдау</button>
          </div>
        </div>
        @endif
        </fieldset>
        </form>
      </div>
    </div>
    <script language="JavaScript" type="text/javascript"
    xml:space="preserve">//<![CDATA[
    //You should create the validator only after the definition of the HTML form
    var frmvalidator  = new Validator("mform");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    @if($lang)
    frmvalidator.addValidation("text","req","Пожалуйста, напишите описание");
    frmvalidator.addValidation("text","maxlen=3000",  "Описание не может быть больше 3000 символов");
    @else
    frmvalidator.addValidation("text","req","Өтініш, сипаттама беріңіз");
    frmvalidator.addValidation("text","maxlen=3000",  "Сипаттама 3000 әріптен аспауы тиіс");
    @endif
  </script>
@stop