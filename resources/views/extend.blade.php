@extends('main')
@section('content')
    <div class='container'>
      <div id='success-notice' class='col-md-12'>
        @if(!$error)
          @if($lang)
          <h2>Ваше объявление успешно продлено!</h2>
          <h4>Вы можете посмотреть ваше объявление по следующей ссылке - <a class='href' href="{!! route('ad', [ $ad->id ]) !!}">посмотреть объявление</a></h4>
          <a href="{!! route('index') !!}" class='btn btn-success'>На главную</a>
          @else
          <h2>Сіздің хабарландыруыңыздың уақыты созылды!</h2>
          <h4>Сіз өз хабарландыруыңызды мына сілтеме бойынша көре аласыз - <a class='href' href="{!! route('ad', [ $ad->id ]) !!}">хабарландыруды көру</a></h4>
          <a href="{!! route('index') !!}" class='btn btn-success'>Басты бет</a>
          @endif
        @else
          @if($lang)
          <h2>Произошла ошибка!</h2>
          <h4>Ссылка устарела.</h4>
          @else
          <h2>Қате орын алды!</h2>
          <h4>Сілтеме ескірген.</h4>
          @endif
        @endif
      </div>
      <div class='col-md-12'>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
      </div>
    </div>
@stop