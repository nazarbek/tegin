@extends('main')
@section('head')
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
  <title>
  	{!! $ad->title !!}
  </title>
@stop
@section('content')
	<div class='container narrow border'>
		<div id='info-nav'>
		<div id='back' class='col-md-2'>
			<a href='{!! URL::previous() !!}' class='btn btn-warning'><i class="fa fa-chevron-left"></i>
				@if($lang)
                  Назад
                @else
                  Артқа
                @endif
			</a>
		</div>
		<div id='path2' class='col-md-10'>
			<a href='/'>
				@if($lang)
                  Главная
                @else
                  Басты бет
                @endif
			<i class='fa fa-angle-double-right'></i></a>
      		<a class='this' href="{!! route('rubrika', [$cat1->id]) !!}">
      			@if($lang) 
      				{!! $cat1->name_ru !!}
      			@else
                	{!! $cat1->name_kz !!}
                @endif
      		<i class='fa fa-angle-double-right'></i></a>
      		@if($cat2)
      		<a class='this' href="{!! route('podrubrika', [$cat1->id, $cat2->id]) !!}">
      			@if($lang) 
      				{!! $cat2->name_ru !!}
      			@else
                	{!! $cat2->name_kz !!}
                @endif
      		<i class='fa fa-angle-double-right'></i></a>
      		@endif
      		<a class='this' href=''> {!! substr($ad->title, 0, 55) !!}...</a>
		</div>
		</div>
	</div>
	<div class='container narrow'>
		<div class='col-md-9'>
			<div id='info-head' class='col-md-12'>
				<h1>{!! $ad->title !!}</h1>
				<p>
				@if($region)
					@if($lang)
						{!! $region->name_ru !!}
					@else
						{!! $region->name_kz !!}
					@endif
				@endif
				@if($city)
				, 
					@if($lang)
						{!! $city->name_ru !!}
					@else
						{!! $city->name_kz !!}
					@endif
				@endif
				</p>
				<span>
					@if($lang)
						Опубликовано в: 
					@else
						Жарияланған уақыты: 
					@endif
					{!! $ad->created_at !!}</span>
			</div>
			<div id='info-c2' class='col-md-12'>
			@if(!empty($images))
			@if($images[0])
				<img src="{{ asset('photos/') }}/{!! $images[0] !!}"></img>
			@else
				<img src="{{ asset('img/no_picture.jpg') }}"></img>
			@endif
			@else
				<img src="{{ asset('img/no_picture.jpg') }}"></img>
			@endif
			</div>
			<div id='description' class='col-md-12'>
				<p>{{ $ad->description }}</p>
			</div>
			<?php $i = 0; ?>
			@foreach($images as $im)
				@if($i>0)
				<div id='info-c2' class='col-md-12 info-image'>
					<img src="{{ asset('photos/') }}/{!! $im !!}"></img>
				</div>
				@endif
				<?php $i++; ?>
			@endforeach
		</div>
		<div id='right' class='col-md-3'>
			<div class='contacts'>
				<h3><i class='fa fa-user'></i> {!! $ad->name !!}</h3>
				@foreach($phone as $p)
			    	<h4>{!! $p->number !!}
			    	<?php
			    	if($p->whatsapp){
			    	?>
			    		<img src="{{ asset('img/whatsapp.png') }}"></img>
			    	<?php
			    	}
			    	if($p->viber){
			    	?>
			    		<img src="{{ asset('img/viber.png') }}"></img>
			    	<?php
			    	} 
			    	?>
			    	</h4>
			    @endforeach
				@if($ad->address)
					<h4 style='margin-top:30px; font-size:15px; font-weight: bold; margin-bottom:20px;'><i class='fa fa-map-marker'></i> {!! $ad->address !!}</h4>
				@endif
				
			</div>
			@if(!($ad->long==''||$ad->lat==''||$ad->long==0||$ad->lat==0))	
			<div id='show-map' style="background-image: url('{{ asset('img/map.png') }}');">
				<p>@if($lang)
					Посмотреть на карте
					@else
					Картада көрсету
					@endif</p>
			</div>
			@endif
			<div class='jaloba'>
				<a href="{!! route('report', [$ad->id]) !!}"><i class='fa fa-send'></i> 
					@if($lang)
					Сообщить о нарушений
					@else
					Шағымдану
					@endif
				</a>
			</div>
		</div>
	</div>
	<input type="hidden" id="lang" value="{!! $lang !!}"/>
	@if(!($ad->long==''||$ad->lat==''||$ad->long==0||$ad->lat==0))
	<!-- Modal -->
	<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">
	        	@if($lang)
				 	Посмотреть на карте
				@else
					Картада көрсету
				@endif
	        </h4>
	      </div>
	      <div class="modal-body">
	        <div id="map" class='info-map'></div>
	      </div>
	    </div>
	  </div>
	</div>
<script type="text/javascript">
  ymaps.ready(init);
  var myMap;
  var lon = {!! $ad->long !!};
  var lat = {!! $ad->lat !!};

  function init () {
    myMap = new ymaps.Map("map", {
        center: [lon, lat],
        zoom: 13
    });
    myPlacemark = new ymaps.Placemark([lon, lat], {
        hintContent: 'Адрес'
    });
    myMap.geoObjects.add(myPlacemark);
  }
</script>
@endif
@stop
