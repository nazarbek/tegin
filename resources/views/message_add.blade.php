@extends('main')
@section('content')
    <div class='container'>
      <div id='success-notice' class='col-md-12'>
        @if($lang)
        <h2>Ваше сообщение успешно отправлено!</h2>
        <h4>Ваше сообщение принято к рассмотрению.</h4>
        <a href="{!! route('index') !!}" class='btn btn-success'>На главную</a>
        @else
        <h2>Сіздің хатыңыз сәтті жолданды!</h2>
        <h4>Біз сіздің ұсынысыңызды қарастыруға тырысамыз.</h4>
        <a href="{!! route('index') !!}" class='btn btn-success'>Басты бет</a>
        @endif
      </div>
      <div class='col-md-12'>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
      </div>
    </div>
@stop