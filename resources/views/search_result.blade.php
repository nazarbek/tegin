@extends('main')
@section('head')
<title>
@if($lang)
  Объявления по результату поиска "{!! $key !!}"
@else
  "{!! $key !!}" іздеу нәтижесі бойынша хабарландырулар
@endif
</title>
@stop
@section('content')
    <div id='filter' class='container'>
      <div id='location' class="col-md-4">
        <button id='show-list' class='btn btn-default'>
          <?php
          if ($lang) {
            if ($region==0) {
              echo "Весь Казахстан";
            }else if($region>0){
              if ($city==0) { ?>
              {!! $allreg->where('id', (int)$region)->first()->name_ru !!}
              <?php
              }else{ ?>
              {!! $allcity->where('id', (int)$city)->first()->name_ru !!}
              <?php
              }
            }
          }else{
            if ($region==0) {
              echo "Барлық Қазақстан";
            }else if($region>0){
              if ($city==0) { ?>
              {!! $allreg->where('id', (int)$region)->first()->name_kz !!}
              <?php
              }else{ ?>
              {!! $allcity->where('id', (int)$city)->first()->name_kz !!}
              <?php
              }
            }
          }?>
          <i class="fa fa-chevron-down"></i></button>
        <div id='loc-div'>
          <ul>
            <li data-region='1' data-city='86' class='loc-item2'><a style='margin-left:4%;' href="{!! route('search') !!}?region=1&city=86&keyword={!! $key !!}">Астана</a></li>
            <li data-region='3' data-city='67' class='loc-item2'><a style='margin-left:4%;' href="{!! route('search') !!}?region=3&city=67&keyword={!! $key !!}">Алматы</a></li>
            @if($lang)
            @foreach($allreg as $r)
              <li data-region='{!! $r->id !!}' class='loc-item'>
                <p>{!! $r->name_ru !!}<i class='fa fa-sort-down'></i></p>
                <div class='loc-sec'>
                  <ul>
                    <li data-region='{!! $r->id !!}' class='loc-item2'><a href="{!! route('search') !!}?region={!! $r->id !!}&keyword={!! $key !!}">{!! $r->name_ru !!}</a></li>
                  @foreach($r->city()->get() as $c)
                    <li data-region='{!! $r->id !!}' data-city='{!! $c->id !!}' class='loc-item2'><a href="{!! route('search') !!}?region={!! $r->id !!}&city={!! $c->id !!}&keyword={!! $key !!}">{!! $c->name_ru !!}</a></li>
                  @endforeach
                  </ul>
                </div>
              </li>
            @endforeach
            @else
            @foreach($allreg as $r)
              <li data-region='{!! $r->id !!}' class='loc-item'>
                <p>{!! $r->name_kz !!}<i class='fa fa-sort-down'></i></p>
                <div class='loc-sec'>
                  <ul>
                    <li data-region='{!! $r->id !!}' class='loc-item2'><a href="{!! route('search') !!}?region={!! $r->id !!}&keyword={!! $key !!}">{!! $r->name_kz !!}</a></li>
                  @foreach($r->city()->get() as $c)
                    <li data-region='{!! $r->id !!}' data-city='{!! $c->id !!}' class='loc-item2'><a href="{!! route('search') !!}?region={!! $r->id !!}&city={!! $c->id !!}&keyword={!! $key !!}">{!! $c->name_kz !!}</a></li>
                  @endforeach
                  </ul>
                </div>
              </li>
            @endforeach
            @endif
          </ul>
        </div>
      </div>
      <div id='search' class="col-md-8">
        <form action="{!! route('search') !!}">
        <input name="keyword" type="text" placeholder="
@if($lang)
Быстрый поиск
@else
Хабарлама іздеу
@endif" class="form-control input-md" required="" value='{!! $key !!}'>
        <button id='search' class='btn btn-default'>
          <i class='fa fa-search'></i>
        </button>
      </form>
      </div>
      <div id='search-rubriks' class="col-md-4">
        <select id="category" name="category1" class="form-control">
          @if($lang)
            <option value='0'>Выберите рубрику</option>
          @else
            <option value='0'>Тақырыпты таңдаңыз</option>
          @endif
          @foreach($allcat1 as $r)
            @if($lang)
              @if($r->id==$cat1)
                <option selected value='{!! $r->id !!}'><a>{!! $r->name_ru !!}</option>
              @else
                <option value='{!! $r->id !!}'><a>{!! $r->name_ru !!}</option>
              @endif
            @else
              @if($r->id==$cat1)
                <option selected value='{!! $r->id !!}'><a>{!! $r->name_kz !!}</option>
              @else
                <option value='{!! $r->id !!}'><a>{!! $r->name_kz !!}</option>
              @endif
            @endif
          @endforeach
        </select>
      </div>
      @if($cat1!=0)
      <div id='search-podrubriks' class="col-md-4">
        <select id="category2" name="category2" class="form-control">
          @if($lang)
            <option value='0'>Выберите подрубрику</option>
          @else
            <option value='0'>Тақырыпшаны таңдаңыз</option>
          @endif
          @foreach($podrubs as $r)
            @if($lang)
              @if($r->id==$cat2)
                <option selected value='{!! $r->id !!}'><a>{!! $r->name_ru !!}</option>
              @else
                <option value='{!! $r->id !!}'><a>{!! $r->name_ru !!}</option>
              @endif
            @else
              @if($r->id==$cat2)
                <option selected value='{!! $r->id !!}'><a>{!! $r->name_kz !!}</option>
              @else
                <option value='{!! $r->id !!}'><a>{!! $r->name_kz !!}</option>
              @endif
            @endif
          @endforeach
        </select>
      </div>
      @endif
    </div>
    <div id='path' class='container'>
      <a href='{!! route("index") !!}'><i class='fa fa-home'></i>
       <?php 
       if($lang){ 
          echo "Главная";
       }else{
          echo "Басты бет";
       } ?>
       <i class='fa fa-angle-double-right'></i></a>
      <a class='this' href=''> 
        <?php if($lang){ ?>
          Результаты поиска "{!! $key !!}"
        <?php }else{ ?>
          Іздеу нәтижесі "{!! $key !!}"
        <?php } ?>
      </a>
    </div>
    <div id='list' class='container'>
      @if($ads->first())
      <hr/>
      @foreach($ads as $ad)
        <div class='col-md-12 advert'>
          <a href="{!! route('ad', [$ad->id]) !!}">
            <div class='advert-left'>
              @if($ad->img1!='')
              <img src='{!! url("photos/".$ad->img1) !!}'></img>
              @elseif($ad->img2!='')
              <img src='{!! url("photos/".$ad->img2) !!}'></img>
              @elseif($ad->img3!='')
              <img src='{!! url("photos/".$ad->img3) !!}'></img>
              @elseif($ad->img4!='')
              <img src='{!! url("photos/".$ad->img4) !!}'></img>
              @else
              <img src='{!! url("img/no_picture.jpg") !!}'></img>
              @endif
            </div>
            <div class='advert-right1'>
              <h3>{!! substr($ad->title, 0, 55) !!}</h3>
              <p>{!! substr($ad->description, 0, 150) !!} ...</p>
              <span>{!! $ad->created_at !!}</span>
              <button href='#' class='btn btn-default'>
                @if($lang)
                  Подробнее
                @else
                  Толығырақ
                @endif
              </button>
            </div>
          </a>
        </div>
      @endforeach
      {!! $ads->links() !!}
      @else
        @if($lang)
          <h4>К сожалению, по этому запросу еще нет объявлений</h4>
        @else
          <h4>Өкінішке орай, осы сұрау бойынша хабарландырулар табылмады</h4>
        @endif
      @endif
    </div>
    <input type="hidden" id="lang" value="{!! $lang !!}"/>
    <script language="JavaScript" type="text/javascript" xml:space="preserve">
      $("#category").change(function(){
        var v = this.value;
          @if($region==0) 
            window.location = "{!! route('search') !!}?cat1="+v+"&keyword={!! $key !!}";
          @else
            @if ($city==0)
              window.location = "{!! route('search') !!}?region={!! $region !!}&cat1="+v+"&keyword={!! $key !!}";
            @else
              window.location = "{!! route('search') !!}?region={!! $region !!}&city={!! $city !!}&cat1="+v+"&keyword={!! $key !!}";
            @endif
          @endif
      });
      $("#category2").change(function(){
        var v = this.value;
          @if($region==0) 
            window.location = "{!! route('search') !!}?cat1={!! $cat1 !!}&cat2="+v+"&keyword={!! $key !!}";
          @else
            @if ($city==0)
              window.location = "{!! route('search') !!}?region={!! $region !!}&cat1={!! $cat1 !!}&cat2="+v+"&keyword={!! $key !!}";
            @else
              window.location = "{!! route('search') !!}?region={!! $region !!}&city={!! $city !!}&cat2="+v+"&cat1={!! $cat1 !!}&keyword={!! $key !!}";
            @endif
          @endif
      });
    </script>
@stop