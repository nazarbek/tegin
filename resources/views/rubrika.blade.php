@extends('main')
@section('head')
    <?php
    $loc='';
      if ($lang) {
        if ($region==0) {
          $loc = "Весь Казахстан";
        }else if($region>0){
          if ($city==0) {
            $loc = $allreg->where('id', (int)$region)->first()->name_ru;
          }else{
            $loc =$allcity->where('id', (int)$city)->first()->name_ru;
          }
      }
      }else{
        if ($region==0) {
          $loc = "Барлық Қазақстан";
        }else if($region>0){
          if ($city==0) {
            $loc =$allreg->where('id', (int)$region)->first()->name_kz;
          }else{
            $loc =$allcity->where('id', (int)$city)->first()->name_kz;
          }
        }
      }?>
  <title>
    @if($lang)
      Объявления отдам даром {!! $cat1->name_ru !!}. {!! $loc !!}
    @else
      Хабарландырулар. Тегін беремін {!! $cat1->name_kz !!}. {!! $loc !!}
    @endif
  </title>
@stop
@section('content')
    <div id='filter' class='container'>
      <div id='location' class="col-md-4">
        <button id='show-list' class='btn btn-default'>
          {!! $loc !!}
          <i class="fa fa-chevron-down"></i></button>
        <div id='loc-div'>
          <ul>
            <li data-region='1' data-city='86' class='loc-item2'><a style='margin-left:4%;' href="{!! route('index') !!}?region=1&city=86">Астана</a></li>
            <li data-region='3' data-city='67' class='loc-item2'><a style='margin-left:4%;' href="{!! route('index') !!}?region=3&city=67">Алматы</a></li>
            @if($lang)
            @foreach($allreg as $r)
              <li data-region='{!! $r->id !!}' class='loc-item'>
                <p>{!! $r->name_ru !!}<i class='fa fa-sort-down'></i></p>
                <div class='loc-sec'>
                  <ul>
                    <li data-region='{!! $r->id !!}' class='loc-item2'><a href="{!! route('rubrika', [$cat1->id]) !!}?region={!! $r->id !!}">{!! $r->name_ru !!}</a></li>
                  @foreach($r->city()->get() as $c)
                    @if($c->id!=86&&$c->id!=67)
                    <li data-region='{!! $r->id !!}' data-city='{!! $c->id !!}' class='loc-item2'><a href="{!! route('rubrika', [$cat1->id]) !!}?region={!! $r->id !!}&city={!! $c->id !!}">{!! $c->name_ru !!}</a></li>
                    @endif
                  @endforeach
                  </ul>
                </div>
              </li>
            @endforeach
            @else
            @foreach($allreg as $r)
              <li data-region='{!! $r->id !!}' class='loc-item'>
                <p>{!! $r->name_kz !!}<i class='fa fa-sort-down'></i></p>
                <div class='loc-sec'>
                  <ul>
                    <li data-region='{!! $r->id !!}' class='loc-item2'><a href="{!! route('rubrika', [$cat1->id]) !!}?region={!! $r->id !!}">{!! $r->name_kz !!}</a></li>
                  @foreach($r->city()->get() as $c)
                    @if($c->id!=86&&$c->id!=67)
                    <li data-region='{!! $r->id !!}' data-city='{!! $c->id !!}' class='loc-item2'><a href="{!! route('rubrika', [$cat1->id]) !!}?region={!! $r->id !!}&city={!! $c->id !!}">{!! $c->name_kz !!}</a></li>
                    @endif
                  @endforeach
                  </ul>
                </div>
              </li>
            @endforeach
            @endif
          </ul>
        </div>
      </div>
      <div id='search' class="col-md-8">
        <form action="{!! route('search') !!}">
        <input name="keyword" type="text" placeholder="
@if($lang)
Быстрый поиск
@else
Хабарлама іздеу
@endif" class="form-control input-md">
        <button id='search' class='btn btn-default'>
          <i class='fa fa-search'></i>
        </button>
      </form>
      </div>
    </div>
    <div id='path' class='container'>
      <a href='{!! route("index") !!}'><i class='fa fa-home'></i>
       <?php 
       if($lang){ 
          echo "Главная";
       }else{
          echo "Басты бет";
       } ?>
       <i class='fa fa-angle-double-right'></i></a>
      <a class='this' href=''> 
        <?php if($lang){ ?>
          {!! $cat1->name_ru !!}
        <?php }else{ ?>
          {!! $cat1->name_kz !!}
        <?php } ?>
      </a>
      <hr/>
    </div>
    <div id='podcategories' class='container'>
      <div class='col-md-3'>
      <?php $i=0; ?>
      @foreach($cat2 as $cat)
        <?php if($lang){ ?>
          <a href="{!! route('podrubrika', [$cat1->id, $cat->id]) !!}">
            {!! $cat->name_ru !!}
          </a>
        <?php }else{ ?>
          <a href='{!! route('podrubrika', [$cat1->id, $cat->id]) !!}'>{!! $cat->name_kz !!}</a>
        <?php } ?>
        <?php $i++; ?>
        <?php if($i%3==0){ ?>
        </div><div class='col-md-3'>
        <?php } ?>
      @endforeach
    </div>
    </div>
    <div id='list' class='container'>
      @if($ads->first())
      @foreach($ads as $ad)
        <div class='col-md-12 advert'>
          <a href="{!! route('ad', [$ad->id]) !!}">
            <div class='advert-left'>
              @if($ad->img1!='')
              <img src='{!! url("photos/".$ad->img1) !!}'></img>
              @elseif($ad->img2!='')
              <img src='{!! url("photos/".$ad->img2) !!}'></img>
              @elseif($ad->img3!='')
              <img src='{!! url("photos/".$ad->img3) !!}'></img>
              @elseif($ad->img4!='')
              <img src='{!! url("photos/".$ad->img4) !!}'></img>
              @else
              <img src='{!! url("img/no_picture.jpg") !!}'></img>
              @endif
            </div>
            <div class='advert-right1'>
              <h3>{!! substr($ad->title, 0, 55) !!}</h3>
              <p>{!! substr($ad->description, 0, 150) !!} ...</p>
              <span>{!! $ad->created_at !!}</span>
              <button href='#' class='btn btn-default'>
                @if($lang)
                  Подробнее
                @else
                  Толығырақ
                @endif
              </button>
            </div>
          </a>
        </div>
      @endforeach
      {!! $ads->links() !!}
      @else
        @if($i>0)
        <hr/>
        @endif
        @if($lang)
          <h4>К сожалению, по этому запросу еще нет объявлений</h4>
        @else
          <h4>Өкінішке орай, осы сұрау бойынша хабарландырулар табылмады</h4>
        @endif
      @endif
    </div>
    <input type="hidden" id="lang" value="{!! $lang !!}"/>
@stop