<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('photos/{image}', function($image){

    //do so other checks here if you wish

    if(!File::exists( $image=storage_path("photos/{$image}") )) abort(404);

    return Image::make($image)->response('jpg'); //will ensure a jpg is always returned
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'MainController@index']);
	Route::get('/new_ad', ['as' => 'new', 'uses' => 'MainController@new_ad']);
	Route::post('/new_ad/add', ['as' => 'add', 'uses' => 'MainController@add']);
	Route::get('/ad/{rubrika}', ['as' => 'rubrika', 'uses' => 'MainController@rubrika']);
	Route::get('/ad/rubrika/{rubrika}/{podrubrika}', ['as' => 'podrubrika', 'uses' => 'MainController@podrubrika']);
	Route::get('/ad/show/{id}', ['as' => 'ad', 'uses' => 'MainController@ad_info']);
	Route::get('/report/{id}', ['as' => 'report', 'uses' => 'MainController@report']);
	Route::get('/report_add', ['as' => 'report_add', 'uses' => 'MainController@report_add']);
	Route::get('/search', ['as' => 'search', 'uses' => 'MainController@search']);
	Route::get('/message', ['as' => 'message', 'uses' => 'MainController@message']);
	Route::get('/message_add', ['as' => 'message_add', 'uses' => 'MainController@message_add']);

	Route::get('/admin', ['as' => 'admin', 'uses' => 'AdminController@admin']);
	Route::get('/login', ['as' => 'login', 'uses' => 'AdminController@login']);
	Route::get('/check', ['as' => 'check', 'uses' => 'AdminController@check']);
	Route::get('/logout', ['as' => 'logout', 'uses' => 'AdminController@logout']);
	Route::get('/reports', ['as' => 'reports', 'uses' => 'AdminController@report']);
	Route::get('/messages', ['as' => 'messages', 'uses' => 'AdminController@message']);

	Route::get('/deleting-time-out-advertisements', ['as' => 'checktimeout', 'uses' => 'MainController@timeout']);
	Route::get('/extend/{id}/{code}', ['as' => 'extend', 'uses' => 'MainController@extend']);
});
