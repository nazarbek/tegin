<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ad;
use App\Cat1;
use App\Cat2;
use App\Phone;
use App\Region;
use App\Report;
use App\Message;
use App\City;
use Session;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function admin(){
		$enter = false;
		if (Session::has('admin_enter')) {
			$enter = (boolean)Session::get('admin_enter');
			if (!$enter) {
				return Redirect::route('login');
			}
		}else{
			return Redirect::route('login');
		}
		$ads = Ad::orderBy('id', 'desc')->take(50)->paginate(20);
		return view('admin', ['ads'=>$ads]);
	}
	public function report(){
		$enter = false;
		if (Session::has('admin_enter')) {
			$enter = (boolean)Session::get('admin_enter');
			if (!$enter) {
				return Redirect::route('login');
			}
		}else{
			return Redirect::route('login');
		}
		$reps = Report::orderBy('id', 'desc')->take(50)->paginate(20);
		return view('admin_report', ['reports'=>$reps]);
	}
	public function message(){
		$enter = false;
		if (Session::has('admin_enter')) {
			$enter = (boolean)Session::get('admin_enter');
			if (!$enter) {
				return Redirect::route('login');
			}
		}else{
			return Redirect::route('login');
		}
		$mes = Message::orderBy('id', 'desc')->take(50)->paginate(20);
		return view('admin_message', ['mes'=>$mes]);
	}
	public function login(){
		$enter = false;
		$error = false;
		if (Session::has('error')) {
			$error = (boolean)Session::get('error');
		}
		if (Session::has('admin_enter')) {
			$enter = (boolean)Session::get('admin_enter');
			if ($enter) {
				return Redirect::route('admin');
			}
		}
		return view('login', ['error'=>$error]);
	}
	public function check(Request $request){
		$login = $request->get('loginq');
		$password = $request->get('passwordq');
		if ($login=='admin'&&$password=='admin') {
			Session::put('admin_enter', true);
			Session::put('error', false);
            Session::save();
			return Redirect::route('admin');
		}else{
			Session::put('error', true);
            Session::save();
			return redirect()->route('login');
		}
	}
	public function logout(){
		Session::put('admin_enter', false);
        Session::save();
		return Redirect::route('login');
	}
}
