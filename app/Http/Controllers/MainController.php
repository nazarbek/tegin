<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ad;
use App\Cat1;
use App\Cat2;
use App\Phone;
use App\Region;
use App\City;
use App\Word;
use App\Report;
use App\Message;
use Session;
use Mail;
use File;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
	public function index(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}

		Session::put('scat1', 0);
		Session::put('scat2', 0);
        Session::save();

		$allreg = Region::all();
		$allcity = City::all();

		$region = $request->region;
		$city = $request->city;
		if(isset($region)&&isset($city)){
			$error = false;
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
				$error=true;
			}
			if ($city<0||$city>110) {
				$city=0;
				$error=true;
			}
			if(!$error){
				Session::put('region', $region);
				Session::put('city', $city);
            	Session::save();
			}
		}else if(isset($region)){
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}else{
				Session::put('region', $region);
				Session::put('city', 0);
            	Session::save();
			}
		}else {
			if (Session::has('region')) {
				$region = (integer)Session::get('region');
			}
			if (Session::has('city')) {
				$city = (integer)Session::get('city');
			}
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}
			if ($city<0||$city>110) {
				$city=0;
			}
		}
		return view('index', ['lang'=>$lang, 'region'=>$region, 'city'=>$city, 'allreg'=>$allreg, 'allcity'=>$allcity]);
	}
	public function rubrika(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}
		
		$rubrika = $request->route('rubrika');

		Session::put('scat1', $rubrika);
        Session::save();

		$cat1 = Cat1::where('id', $rubrika)->first();
		$cat2 = Cat2::where('cat1_id', $rubrika)->get();
		$allreg = Region::all();
		$allcity = City::all();
		$region = $request->region;
		$city = $request->city;
		if(isset($region)&&isset($city)){
			$error = false;
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
				$error=true;
			}
			if ($city<0||$city>110) {
				$city=0;
				$error=true;
			}
			if(!$error){
				Session::put('region', $region);
				Session::put('city', $city);
            	Session::save();
			}
		}else if(isset($region)){
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}else{
				Session::put('region', $region);
				Session::put('city', 0);
            	Session::save();
			}
		}else {
			if (Session::has('region')) {
				$region = (integer)Session::get('region');
			}
			if (Session::has('city')) {
				$city = (integer)Session::get('city');
			}
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}
			if ($city<0||$city>110) {
				$city=0;
			}
		}
		$ads = null;
		if($region!=0||$city!=0){
			if ($city!=0) {
				$ads = $cat1->ads()->where('city_id', $city)->paginate(5);
			}else{
				$ads = $cat1->ads()->where('region_id', $region)->paginate(5);
			}
		}else{
			$ads = $cat1->ads()->paginate(5);
		}
        return view('rubrika', ['ads' => $ads, 'cat2'=> $cat2, 'cat1'=> $cat1, 'lang'=>$lang, 'region'=>$region, 'city'=>$city, 'allreg'=>$allreg, 'allcity'=>$allcity]);
	}
	public function podrubrika(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}
		$rubrika = $request->route('rubrika');
		$podrubrika = $request->route('podrubrika');

		Session::put('scat1', $rubrika);
		Session::put('scat2', $podrubrika);
        Session::save();

		$cat1 = Cat1::where('id', $rubrika)->first();
		$cat2 = Cat2::where('id', $podrubrika)->first();
		$allreg = Region::all();
		$allcity = City::all();

		$region = $request->region;
		$city = $request->city;
		if(isset($region)&&isset($city)){
			$error = false;
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
				$error=true;
			}
			if ($city<0||$city>110) {
				$city=0;
				$error=true;
			}
			if(!$error){
				Session::put('region', $region);
				Session::put('city', $city);
            	Session::save();
			}
		}else if(isset($region)){
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}else{
				Session::put('region', $region);
				Session::put('city', 0);
            	Session::save();
			}
		}else {
			if (Session::has('region')) {
				$region = (integer)Session::get('region');
			}
			if (Session::has('city')) {
				$city = (integer)Session::get('city');
			}
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}
			if ($city<0||$city>110) {
				$city=0;
			}
		}
		$ads = null;
		if($region!=0||$city!=0){
			if ($city!=0) {
				$ads = $cat2->ads()->where('city_id', $city)->paginate(5);
				
			}else{
				$ads = $cat2->ads()->where('region_id', $region)->paginate(5);
			}
		}else{
			$ads = $cat2->ads()->paginate(5);
		}
        return view('podrubrika', ['ads' => $ads, 'cat2'=> $cat2, 'cat1'=> $cat1, 'lang'=>$lang, 'region'=>$region, 'city'=>$city, 'allreg'=>$allreg, 'allcity'=>$allcity]);
	}
	public function ad_info(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}

		$id = $request->route('id');
		$thisad = Ad::where('id', $id)->first();
		$cat1 = Cat1::where('id', $thisad->cat1_id)->first();
		$cat2 = Cat2::where('id', $thisad->cat2_id)->first();
		$phone = Phone::where('ad_id', $id)->get();
		$city=null;
		$region=null;
		if($thisad->city_id!=0){
			$region=Region::where('id', $thisad->region_id)->first();
			$city=City::where('id', $thisad->city_id)->first();
		}else{
			$region=Region::where('id', $thisad->region_id)->first();
		}
		$images = array();
		if ($thisad->img1!='') {
			array_push($images, $thisad->img1);
		}
		if ($thisad->img2!='') {
			array_push($images, $thisad->img2);
		}
		if ($thisad->img3!='') {
			array_push($images, $thisad->img3);
		}
		if ($thisad->img4!='') {
			array_push($images, $thisad->img4);
		}
        return view('ad_info', ['ad' => $thisad, 'lang'=>$lang, 'cat2'=> $cat2, 'cat1'=> $cat1, 'phone' => $phone, 'region'=> $region, 'city'=> $city, 'images'=>$images]);
	}
    public function new_ad(Request $request){
    	$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}
		$rubrika = Cat1::all();
		$podrubrika = Cat2::all();
    	$reg = Region::all();
    	$city = City::all();
    	$word = Word::where('group', 2)->get();
	    return view('new_ad', ['reg' => $reg, 'city' => $city, 'lang' => $lang, 'word'=>$word, 'rubrika'=>$rubrika, 'podrubrika'=>$podrubrika]);
	}
	public function add(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}

		$error = false;

		$title = $request->get('title');
		$cat1 = $request->get('category1');
		$cat2 = $request->get('category2');
		$description = $request->get('description');
		$name = $request->get('name');
		$region = $request->get('region');
		$city = $request->get('city');
		$address = $request->get('address');
		$phone = $request->get('phone');
		$whatsapp= $request->get('whatsapp');
		$viber = $request->get('viber');
		$email = $request->get('email');
		$long = $request->get('long');
		$lat = $request->get('lat');
		$ad = new Ad;
		if($request->hasFile('file1')){
			$file1 = $request->file('file1');
        	$file_path1=date('YmdHis').'1';
        	$file1->move(public_path('photos'), $file_path1);
        	$ad->img1=$file_path1;
		}
		if($request->hasFile('file2')){
			$file2 = $request->file('file2');
        	$file_path2=date('YmdHis').'2';
        	$file2->move(public_path('photos'), $file_path2);
        	$ad->img2=$file_path2;
		}
		if($request->hasFile('file3')){
			$file3 = $request->file('file3');
        	$file_path3=date('YmdHis').'3';
        	$file3->move(public_path('photos'), $file_path3);
        	$ad->img3=$file_path3;
		}
		if($request->hasFile('file4')){
			$file4 = $request->file('file4');
        	$file_path4=date('YmdHis').'4';
        	$file4->move(public_path('photos'), $file_path4);
        	$ad->img4=$file_path4;
		}
		if(!isset($title)){
			$error=true;
		}
		if($cat1>15||$cat1==0){
			$error=true;
		}
		if(!isset($description)){
			$error=true;
		}
		if(!isset($name)){
			$error=true;
		}
		if($region==0){
			$error=true;
		}
		if(!isset($email)){
			$error=true;
		}
		if (!$error) {
	        $ad->title=$title;
	        $ad->cat1_id=$cat1;
	        if($cat1==14||$cat1==13){
	        	$ad->cat2_id=0;
	        }else{
	        	$ad->cat2_id=$cat2;
	        }
	        $ad->description=$description;
	        $ad->name=$name;
	        $ad->region_id=$region;
	        $ad->city_id=$city;
	        $ad->address=$address;
	        $ad->email=$email;
	        $ad->long=$long;
	        $ad->lat=$lat;
	        $ad->save();
	        for($i=1; $i<=count($phone); $i++) {
	        	if($phone[$i]!=''){
	        		$p = new Phone;
					$p->number=$phone[$i];
					if (isset($whatsapp)) {
						foreach ($whatsapp as $w) {
							if ($i==$w) {
								$p->whatsapp=true;
							}
						}
					}
					if (isset($viber)) {
						foreach ($viber as $v) {
							if ($i==$v) {
								$p->viber=true;
							}
						}
					}
					$p->ad_id=$ad->id;
					$p->save();
	        	}
			}
		}
		return view('success', ['lang'=>$lang, 'ad_id'=> $ad->id, 'error'=>$error]);
	}
	public function report(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}
		$id = $request->route('id');
		return view('report', ['lang'=>$lang, 'ad_id'=> $id]);
	}
	public function report_add(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}
		$id = $request->get('report_submit');
		$text = $request->get('text');
		$r = new Report;
		$r->text=$text;
		$r->ad_id=$id;
        $r->save();
		return view('report_add', ['lang'=>$lang]);
	}
	public function message(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}
		return view('message', ['lang'=>$lang]);
	}
	public function message_add(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}
		$name = $request->get('name');
		$email = $request->get('email');
		$text = $request->get('text');
		$r = new Message;
		$r->name=$name;
		$r->text=$text;
		$r->email=$email;
        $r->save();

        Mail::send('message_email', ['text'=>$text, 'name'=>$name, 'email'=>$email], function ($message) use ($email, $name) {
		    $message->sender(''.$email, ''.$name);
		    $message->subject('С сайта tegyn.kz');
		    $message->to('tegyn.info@gmail.com');
		});
		return view('message_add', ['lang'=>$lang]);
	}
	public function search(Request $request){
		$key = $request->get('keyword');
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}

		$cat1 = 0;
		$cat2 = 0;
		$podrubs = null;
		if (isset($request->cat1)&&isset($request->cat2)) {
			$cat1 = $request->cat1;
			$cat2 = $request->cat2;
			Session::put('scat1', $cat1);
			Session::put('scat2', $cat2);
            Session::save();
            if ($cat1!=0) {
            	$podrubs = Cat1::where('id', $cat1)->first()->cat2()->get();
            }
		}else if(isset($request->cat1)){
			$cat1 = $request->cat1;
			Session::put('scat1', $cat1);
            Session::save();
            if ($cat1!=0) {
            	$podrubs = Cat1::where('id', $cat1)->first()->cat2()->get();
            }
		}else{
			if (Session::has('scat1')) {
				$cat1 = (integer)Session::get('scat1');
				if (Session::has('scat2')) {
					$cat2 = (integer)Session::get('scat2');
				}
				if ($cat1!=0) {
            		$podrubs = Cat1::where('id', $cat1)->first()->cat2()->get();
            	}
			}
		}
		$allcat1 = Cat1::all();

		$allreg = Region::all();
		$allcity = City::all();

		$region = $request->region;
		$city = $request->city;
		if(isset($region)&&isset($city)){
			$error = false;
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
				$error=true;
			}
			if ($city<0||$city>310) {
				$city=0;
				$error=true;
			}
			if(!$error){
				Session::put('region', $region);
				Session::put('city', $city);
            	Session::save();
			}
		}else if(isset($region)){
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}else{
				Session::put('region', $region);
				Session::put('city', 0);
            	Session::save();
			}
		}else {
			if (Session::has('region')) {
				$region = (integer)Session::get('region');
			}
			if (Session::has('city')) {
				$city = (integer)Session::get('city');
			}
			if ($region<0||$region>14) {
				$region=0;
				$city=0;
			}
			if ($city<0||$city>110) {
				$city=0;
			}
		}
		$ads = null;
		if ($cat1!=0 || $cat2!=0) {
			if ($cat2!=0) {
				if($region!=0||$city!=0){
					if ($city!=0) {
						$ads = Ad::where('city_id', $city.'')->where('cat2_id', $cat2)->where(
							function ($query) use ($key){
		                		$query->where('title', 'LIKE', '%'.$key.'%')
		                      	->orWhere('description', 'LIKE', '%'.$key.'%');
		            		}
						);
					}else{
						$ads = Ad::where('region_id', $region.'')->where('cat2_id', $cat2)->where(
							function ($query) use ($key){
		                		$query->where('title', 'LIKE', '%'.$key.'%')
		                      	->orWhere('description', 'LIKE', '%'.$key.'%');
		            		}
						);
					}
				}else{
					$ads = Ad::where('cat2_id', $cat2.'')->where(
						function ($query) use ($key){
		                	$query->where('title', 'LIKE', '%'.$key.'%')
		                      	->orWhere('description', 'LIKE', '%'.$key.'%');
		            	}
					);
				}
			}else{
				if($region!=0||$city!=0){
					if ($city!=0) {
						$ads = Ad::where('city_id', $city.'')->where('cat1_id', $cat1)->where(
							function ($query) use ($key){
		                		$query->where('title', 'LIKE', '%'.$key.'%')
		                      	->orWhere('description', 'LIKE', '%'.$key.'%');
		            		}
						);
					}else{
						$ads = Ad::where('region_id', $region.'')->where('cat1_id', $cat1)->where(
							function ($query) use ($key){
		                		$query->where('title', 'LIKE', '%'.$key.'%')
		                      	->orWhere('description', 'LIKE', '%'.$key.'%');
		            		}
						);
					}
				}else{
					$ads = Ad::where('cat1_id', $cat1.'')->where(
						function ($query) use ($key){
		                	$query->where('title', 'LIKE', '%'.$key.'%')
		                      	->orWhere('description', 'LIKE', '%'.$key.'%');
		            	}
					);
				}
			}
		}else{
			if($region!=0||$city!=0){
				if ($city!=0) {
					$ads = Ad::where('city_id', $city.'')->where(
						function ($query) use ($key){
		                	$query->where('title', 'LIKE', '%'.$key.'%')
		                    ->orWhere('description', 'LIKE', '%'.$key.'%');
		            	}
					);
				}else{
					$ads = Ad::where('region_id', $region.'')->where(
						function ($query) use ($key){
		                	$query->where('title', 'LIKE', '%'.$key.'%')
		                    ->orWhere('description', 'LIKE', '%'.$key.'%');
		            	}
					);
				}
			}else{
				$ads = Ad::where('title', 'LIKE', '%'.$key.'%')->orWhere('description', 'LIKE', '%'.$key.'%');
			}
		}
		$ads = $ads->paginate(5);
        return view('search_result', ['ads' => $ads, 'key'=>$key,'lang'=>$lang, 'region'=>$region, 'city'=>$city, 'allreg'=>$allreg, 'allcity'=>$allcity, 'allcat1'=>$allcat1, 'cat1'=>$cat1, 'cat2'=>$cat2, 'podrubs'=>$podrubs]);
	}

	public function timeout(){
		$ads = Ad::all();
		$now = Carbon::now();
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		foreach ($ads as $ad) {
			$created = new Carbon($ad->created_at);
			if ($created->diff($now)->days > 23 && $created->diff($now)->days < 31) {
				if (!$ad->sent) {
					$code = '';
					for ($i = 0; $i < 10; $i++) {
	        			$code .= $characters[rand(0, $charactersLength - 1)];
	    			}
					$id = $ad->id;
					$email = $ad->email;
					$name = $ad->name;
					$ad->sent = true;
					$ad->code = $code;
					$ad->save();
					Mail::send('email_timeout', ['name'=>$name, 'ad'=>$ad, 'code'=>$code], function ($message) use ($email, $name) {
					    $message->sender(''.$email, ''.$name);
					    $message->subject('Обновите свое объявление. tegyn.kz');
					    $message->to($email);
					});
				}
			}else if ($created->diff($now)->days > 30) {
				if($ad->img1!=''){
					File::delete(public_path("photos/".$ad->img1));
				}
				if($ad->img2!=''){
					File::delete(public_path("photos/".$ad->img2));
				}
				$ad->delete();
			}
		}
	}
	public function extend(Request $request){
		$lang=true;
		if(isset($request->lang)){
			if($request->lang=='ru'){
				$lang=true;
				Session::put('lang', true);
            	Session::save();
			}else if($request->lang=='kz'){
				$lang=false;
				Session::put('lang', false);
            	Session::save();
			}
		}else{
			if (Session::has('lang')) {
				$lang = (boolean)Session::get('lang');
			}
		}

		$id = $request->route('id');
		$code = $request->route('code');
		$ad = Ad::find($id);
		$error = false;
		if ($ad) {
			if($code == $ad->code){
				$ad->sent = false;
				$ad->created_at = Carbon::now();
				$ad->code = 'noooo';
				$ad->save();
			}else{
				$error=true;
			}
		}else{
			$error = true;
		}
		return view('extend', ['lang'=>$lang, 'error'=>$error, 'ad'=>$ad]);
	}
}
