<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat2 extends Model
{
	public function ads(){
    	return $this->hasMany('App\Ad');
    }
    public function cat1(){
    	return $this->belongsTo('App\Cat1');
    }
}
