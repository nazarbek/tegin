<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cat1 extends Model
{
    public function ads(){
    	return $this->hasMany('App\Ad');
    }
    public function cat2(){
    	return $this->hasMany('App\Cat2');
    }
}
