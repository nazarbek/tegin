<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
	public function phone(){
    	return $this->hasMany('App\Phone');
    }
    public function Cat1(){
    	return $this->belongsTo('App\Cat1');
    }
    public function Cat2(){
    	return $this->belongsTo('App\Cat2');
    }
}
