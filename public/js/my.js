$( document ).ready(function() {
    var lang = document.getElementById('lang').value;
    /*
    function readURL(input, num) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                //$('#blah').attr('src', e.target.result);
                //$('#fin1').css({'background': e.target.result});
                if(num==1){
                    $("#fin1").css("background-image", "url('"+e.target.result+"')");
                }else if(num==2){
                    $("#fin2").css("background-image", "url('"+e.target.result+"')");
                }else if(num==3){
                    $("#fin3").css("background-image", "url('"+e.target.result+"')");
                }else if(num==4){
                    $("#fin4").css("background-image", "url('"+e.target.result+"')");
                }
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#fin-b1").change(function(){
        readURL(this, 1);
    });
    $("#fin-b2").change(function(){
        readURL(this, 2);
    });
    $("#fin-b3").change(function(){
        readURL(this, 3);
    });
    $("#fin-b4").change(function(){
        readURL(this, 4);
    });
    */
    var phone_id=1;
    $("#add_new_num").click(function(){
        phone_id = phone_id + 1;
        $('#phone-container').append("<div style='margin-bottom: 8px;' class='form-group'><label class='col-md-2 control-label' for='phone1'>Телефон</label><div class='col-md-3'>"+
          "<input class='phone_input' name='phone["+phone_id+"]' type='text' class='form-control input-md' ></div><div class='col-md-3'>"+
          "<label class='checkbox-inline' for='checkboxes-"+phone_id+"'><input type='checkbox' name='whatsapp["+phone_id+"]' value='"+phone_id+"'>Whatsapp</label>"+
          "<label class='checkbox-inline' for='checkboxes-"+phone_id+"'>"+
        "<input type='checkbox' name='viber["+phone_id+"]' value='"+phone_id+"'>Viber</label></div></div>");
        $(".phone_input").mask("+9(999) 999-99-99");
    });
    $(".phone_input").mask("+9(999) 999-99-99");

    

    $("#oblast").change(function(){
        var v = this.value;
        if(v==0){
            $('#gorod').prop("disabled", true);
        }else if(v>0&&v<15){
            var s = document.getElementById('hcity'+v).value;
            var s2 = document.getElementById('hcid'+v).value;
            var arr = s.split('=');
            var arr2 = s2.split('=');
            var con = "<option value='0'>Выбрать город</option>";
            if (!lang) {
                con = "<option value='0'>Қаланы таңдау</option>";
            }
            
            for (var i = 1; i<arr.length; i++) {
                con+="<option value='"+arr2[i]+"'>"+arr[i]+"</option>";
            }
            $('#gorod').html(con);
            $('#gorod').prop("disabled", false);
        }
    });

    var show=true;
    $("#show-list").click(function(){
        if (show) {
            document.getElementById('loc-div').style.display='block';
            show=false;
        }else{
            document.getElementById('loc-div').style.display='none';
            show=true;
        }
    });
    var reg_arr = [false, false, false, false, false, false, false, false, false, false, false, false];
    $(".loc-item").click(
        function(){
            var reg = $(this).attr("data-region");
            if(reg_arr[reg]){
                this.getElementsByTagName('div')[0].style.display='none';
                reg_arr[reg]=false;
            }else{
                this.getElementsByTagName('div')[0].style.display='block';
                reg_arr[reg]=true;
            }
        }
    );

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }

    $("#show-map").click(function(){
        $('#myModal').modal('show');
    });
    
});